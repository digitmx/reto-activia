<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invite extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;

		//Check Session
		//if (!$logged) { redirect( base_url() ); }
	}
	
	public function index($invite = '')
	{
		if ($invite)
		{
			//Read Invite
			$data['invite'] = $invite;
			$this->session->set_userdata('invite', $invite);
			
			//Facebook
			$data['facebook'] = $this->facebook->loginUrl();
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('invite/index', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Redirect to Home
			redirect( base_url() );
		}
	}
}