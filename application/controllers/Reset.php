<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}
	
	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('reset/index');
		$this->load->view('includes/footer');
	}
	
	public function mail()
	{
		//Load View
		$this->load->view('mail/reset');
	}
	
	public function token()
	{
		//Leemos el Token
		$token = $this->uri->segment(3,0);
		
		//Consultamos el Token
		$query = $this->db->query("SELECT * FROM user WHERE token = '" . $token . "' AND status = 1 LIMIT 1");
		
		//Verificamos la consulta
		if ($query->num_rows() > 0)
		{
			//Asignamos el Token
			$data['token'] = $token;
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('reset/token', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Redirigimos al HOME
			redirect( base_url() );
		}
	}
}
