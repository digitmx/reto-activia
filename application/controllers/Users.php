<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'users/dashboard' ); }
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('users/index');
		$this->load->view('includes/footer');
	}
	
	public function dashboard()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.createdAt BETWEEN '2016-12-01 00:00:00' AND '2017-01-15 23:59:59' AND log.status = 1 GROUP BY log.iduser ORDER BY points DESC LIMIT 0,20");
		$data['cero'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-01-22 23:59:59' AND log.status = 1 GROUP BY log.iduser ORDER BY points DESC");
		$data['uno'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.createdAt BETWEEN '2017-01-23 00:00:00' AND '2017-01-29 23:59:59' AND log.status = 1 GROUP BY log.iduser ORDER BY points DESC");
		$data['dos'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.createdAt BETWEEN '2017-01-30 00:00:00' AND '2017-02-05 23:59:59' AND log.status = 1 GROUP BY log.iduser ORDER BY points DESC");
		$data['tres'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.createdAt BETWEEN '2017-02-06 00:00:00' AND '2017-02-12 23:59:59' AND log.status = 1 GROUP BY log.iduser ORDER BY points DESC");
		$data['cuatro'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT log.iduser, (SELECT name FROM user WHERE log.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE log.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE log.iduser = user.iduser) as email, SUM(log.points) as points FROM log WHERE log.status = 1 GROUP BY log.iduser ORDER BY points DESC LIMIT 50");
		$data['ranking'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('users/dashboard', $data);
		$this->load->view('includes/footer');
	}
	
	public function records()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Leemos los Usuarios Registrados
		$query_all = $this->db->query("SELECT * FROM user WHERE user.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-02-12 23:59:59' AND user.status = 1");
		$data['user_all'] = $query_all->num_rows();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('users/records', $data);
		$this->load->view('includes/footer');
	}
	
	public function download()
	{
		$date = $this->uri->segment(3,0);
		$query = $this->db->query("SELECT * FROM user WHERE user.createdAt BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' AND user.status = 1");
		
		foreach ($query->result() as $usuario)
		{
			$status = $usuario->status;
			$verified = $usuario->verified;
			
			$results[] = array(
				'ID' => $usuario->iduser,
				'Nombre' => $usuario->name,
				'Apellidos' => $usuario->lastname,
				'Email' => $usuario->email,
				'Confirmado' => ($verified) ? 'Si' : 'No',
				'FechaRegistro' => $usuario->createdAt
			);
		}
		
		$query_usuarios = $results;
		
		// Set header row values
		$csv_fields=array();
		$csv_fields[] = 'ID';
		$csv_fields[] = 'Nombre';
		$csv_fields[] = 'Apellidos';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Confirmado';
		$csv_fields[] = 'FechaRegistro';
		$output_filename = 'reportes/reporte-'.$date.'.csv';
		$output_handle = @fopen( 'reportes/reporte-'.$date.'.csv', 'w' );

		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( 'Content-Disposition: attachment; filename=' . $output_filename );
		header( 'Expires: 0' );
		header( 'Pragma: public' );

		// Insert header row
		fputcsv( $output_handle, $csv_fields );

		// Parse results to csv format
		foreach ($query_usuarios as $Result)
		{
			$leadArray = (array) $Result; // Cast the Object to an array
			// Add row to file
			$leadArray = array_map("utf8_decode", $leadArray);
			fputcsv( $output_handle, $leadArray );
		}

		// Close output file stream
		fclose( $output_handle );
		
		readfile($output_filename);
	}
	
	public function codes()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Leemos los Usuarios Registrados
		$query_all = $this->db->query("SELECT * FROM log WHERE log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-02-12 23:59:59' AND log.status = 1");
		$data['codes_all'] = $query_all->num_rows();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('codes/index', $data);
		$this->load->view('includes/footer');
	}
	
	public function download_codes()
	{
		$date = $this->uri->segment(3,0);
		$query = $this->db->query("SELECT (SELECT CONCAT(name,' ',lastname) FROM user WHERE user.iduser = log.iduser) as Nombre, (SELECT email FROM user WHERE user.iduser = log.iduser) as Email, log.value as Codigo, log.ip as IP, log.createdAt as FechaRegistro, log.product as Producto FROM log WHERE createdAt BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' AND log.action != 'invite'");
		
		foreach ($query->result() as $log)
		{
			$results[] = array(
				'Nombre' => $log->Nombre,
				'Email' => $log->Email,
				'Codigo' => $log->Codigo,
				'Producto' => $log->Producto,
				'IP' => $log->IP,
				'FechaRegistro' => $log->FechaRegistro
			);
		}
		
		$query_logs = $results;
		
		// Set header row values
		$csv_fields=array();
		$csv_fields[] = 'Nombre';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Codigo';
		$csv_fields[] = 'Producto';
		$csv_fields[] = 'IP';
		$csv_fields[] = 'FechaRegistro';
		$output_filename = 'reportes/codigos-'.$date.'.csv';
		$output_handle = @fopen( 'reportes/codigos-'.$date.'.csv', 'w' );

		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( 'Content-Disposition: attachment; filename=' . $output_filename );
		header( 'Expires: 0' );
		header( 'Pragma: public' );

		// Insert header row
		fputcsv( $output_handle, $csv_fields );

		// Parse results to csv format
		foreach ($query_logs as $Result)
		{
			$leadArray = (array) $Result; // Cast the Object to an array
			// Add row to file
			$leadArray = array_map("utf8_decode", $leadArray);
			fputcsv( $output_handle, $leadArray );
		}

		// Close output file stream
		fclose( $output_handle );
		
		readfile($output_filename);
	}
	
	public function assign()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT * FROM user WHERE status = 1 ORDER BY name, lastname ASC");
		$data['users'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('users/assign', $data);
		$this->load->view('includes/footer');
	}
	
	public function week()
	{
		//Leemos los datos
		$iduser = $this->uri->segment(3,0);
		$week = $this->uri->segment(4,0);
		
		//Verificamos
		if ($iduser && $week)
		{
			//Switch Dates
			switch ((string)$week)
			{
				case '1': $week_string = "log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-01-22 23:59:59'"; break;
				case '2': $week_string = "log.createdAt BETWEEN '2017-01-23 00:00:00' AND '2017-01-29 23:59:59'"; break;
				case '3': $week_string = "log.createdAt BETWEEN '2017-01-30 00:00:00' AND '2017-02-05 23:59:59'"; break;
				case '4': $week_string = "log.createdAt BETWEEN '2017-02-06 00:00:00' AND '2017-02-12 23:59:59'"; break;
				default: $week_string = "log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-01-22 23:59:59'"; break;
			}
			
			//Generamos la Consulta
			$query = $this->db->query("SELECT (SELECT CONCAT(name,' ',lastname) FROM user WHERE user.iduser = log.iduser) as Nombre, (SELECT email FROM user WHERE user.iduser = log.iduser) as Email, log.value as Codigo, log.points as Puntos, log.product as Producto, log.ip as IP, log.createdAt as FechaRegistro FROM log WHERE log.iduser = " . $iduser . " AND log.status = 1 AND " . $week_string);
			
			foreach ($query->result() as $log)
			{
				$results[] = array(
					'Nombre' => $log->Nombre,
					'Email' => $log->Email,
					'Codigo' => $log->Codigo,
					'Puntos' => $log->Puntos,
					'Producto' => $log->Producto,
					'IP' => $log->IP,
					'FechaRegistro' => $log->FechaRegistro
				);
				
				$email = $log->Email;
			}
			
			$query_logs = $results;
			
			// Set header row values
			$csv_fields=array();
			$csv_fields[] = 'Nombre';
			$csv_fields[] = 'Email';
			$csv_fields[] = 'Codigo';
			$csv_fields[] = 'Puntos';
			$csv_fields[] = 'Producto';
			$csv_fields[] = 'IP';
			$csv_fields[] = 'FechaRegistro';
			$output_filename = 'reportes/'.$email.'_semana'.$week.'.csv';
			$output_handle = @fopen( 'reportes/'.$email.'_semana'.$week.'.csv', 'w' );
	
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-type: text/csv' );
			header( 'Content-Disposition: attachment; filename=' . $output_filename );
			header( 'Expires: 0' );
			header( 'Pragma: public' );
	
			// Insert header row
			fputcsv( $output_handle, $csv_fields );
	
			// Parse results to csv format
			foreach ($query_logs as $Result)
			{
				$leadArray = (array) $Result; // Cast the Object to an array
				// Add row to file
				$leadArray = array_map("utf8_decode", $leadArray);
				fputcsv( $output_handle, $leadArray );
			}
	
			// Close output file stream
			fclose( $output_handle );
			
			readfile($output_filename);
		}
		else
		{
			//Error
			redirect( base_url().'users' );
		}
	}
	
	public function ranking()
	{
		//Leemos los datos
		$iduser = $this->uri->segment(3,0);
		
		//Verificamos
		if ($iduser)
		{
			//Generamos la Consulta
			$query = $this->db->query("SELECT (SELECT CONCAT(name,' ',lastname) FROM user WHERE user.iduser = log.iduser) as Nombre, (SELECT email FROM user WHERE user.iduser = log.iduser) as Email, log.value as Codigo, log.points as Puntos, log.product as Producto, log.ip as IP, log.createdAt as FechaRegistro FROM log WHERE log.iduser = " . $iduser . " AND log.status = 1");
			
			foreach ($query->result() as $log)
			{
				$results[] = array(
					'Nombre' => $log->Nombre,
					'Email' => $log->Email,
					'Codigo' => $log->Codigo,
					'Puntos' => $log->Puntos,
					'Producto' => $log->Producto,
					'IP' => $log->IP,
					'FechaRegistro' => $log->FechaRegistro
				);
				
				$email = $log->Email;
			}
			
			$query_logs = $results;
			
			// Set header row values
			$csv_fields=array();
			$csv_fields[] = 'Nombre';
			$csv_fields[] = 'Email';
			$csv_fields[] = 'Codigo';
			$csv_fields[] = 'Puntos';
			$csv_fields[] = 'Producto';
			$csv_fields[] = 'IP';
			$csv_fields[] = 'FechaRegistro';
			$output_filename = 'reportes/'.$email.'_ranking.csv';
			$output_handle = @fopen( 'reportes/'.$email.'_ranking.csv', 'w' );
	
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-type: text/csv' );
			header( 'Content-Disposition: attachment; filename=' . $output_filename );
			header( 'Expires: 0' );
			header( 'Pragma: public' );
	
			// Insert header row
			fputcsv( $output_handle, $csv_fields );
	
			// Parse results to csv format
			foreach ($query_logs as $Result)
			{
				$leadArray = (array) $Result; // Cast the Object to an array
				// Add row to file
				$leadArray = array_map("utf8_decode", $leadArray);
				fputcsv( $output_handle, $leadArray );
			}
	
			// Close output file stream
			fclose( $output_handle );
			
			readfile($output_filename);
		}
		else
		{
			//Error
			redirect( base_url().'users' );
		}
	}
	
	public function valid()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Leemos los Códigos Registrados
		$query_l10 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L10' AND code.status = 1");
		$data['code_l10'] = $query_l10->result();
		
		//Leemos los Códigos Registrados
		$query_l14 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L14' AND code.status = 1");
		$data['code_l14'] = $query_l14->result();
		
		//Leemos los Códigos Registrados
		$query_l15 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L15' AND code.status = 1");
		$data['code_l15'] = $query_l15->result();
		
		//Leemos los Códigos Registrados
		$query_l17 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L17' AND code.status = 1");
		$data['code_l17'] = $query_l17->result();
		
		//Leemos los Códigos Registrados
		$query_l21 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L21' AND code.status = 1");
		$data['code_l21'] = $query_l21->result();
		
		//Leemos los Códigos Registrados
		$query_l22 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L22' AND code.status = 1");
		$data['code_l22'] = $query_l22->result();
		
		//Leemos los Códigos Registrados
		$query_l23 = $this->db->query("SELECT * FROM code WHERE code.folio = 'L23' AND code.status = 1");
		$data['code_l23'] = $query_l23->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('users/valid', $data);
		$this->load->view('includes/footer');
	}
	
	public function new_code()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Leemos los Datos
		$date = (isset($_POST['inputDate'])) ? (string)trim($_POST['inputDate']) : '';
		$folio = (isset($_POST['folio'])) ? (string)trim($_POST['folio']) : '';
		$start = (isset($_POST['start'])) ? (string)trim($_POST['start']) : '';
		$end = (isset($_POST['end'])) ? (string)trim($_POST['end']) : '';
		$points = (isset($_POST['points'])) ? (string)trim($_POST['points']) : '';
		$product = (isset($_POST['product'])) ? (string)trim($_POST['product']) : '';

		//Verificamos
		if ($date && $folio && $start && $end && $points && $product)
		{
			//Generamos el Registro
			$data = array(
				'date' => $date,
				'folio' => $folio,
				'start' => $start,
				'end' => $end,
				'points' => $points,
				'product' => $product,
				'createdAt' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			$this->db->insert('code', $data);
		}
		
		redirect( base_url().'users/valid' );
	}
	
}