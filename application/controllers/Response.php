<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response extends CI_Controller {

	public function index()
	{
		//Check Facebook Response
		$response = $this->facebook->response();
		$params = ($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';
		$ip = '0.0.0.0';
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}
		
		//Check Response
		if (is_array($response))
		{
			//Check if exists Facebook User
			$this->db->where('fbid', $response['id']);
			$this->db->limit(1);
			$query_facebook = $this->db->get('user');
			
			//Check 
			if ($query_facebook->num_rows() > 0)
			{
				//Read User Data
				$row = $query_facebook->row();
				
				//Build User Array
				$data = array(
					'iduser' => $row->iduser,
					'email' => $row->email,
				    'name' => $row->name,
				    'lastname' => $row->lastname,
				    'code' => $row->code,
				    'verified' => $row->verified
				);

				//Save Admin in $_SESSION
				$this->session->set_userdata('user', $data);
				$this->session->set_userdata('ra_logged', true);
				
				//Redirect to Home
				redirect(base_url().'dashboard/');
			}
			else
			{
				//Check Facebook Email with Users
				$this->db->where('email', (string)trim($response['email']));
				$this->db->limit(1);
				$query_email = $this->db->get('user');
				
				//Check 
				if ($query_email->num_rows() > 0)
				{
					//Read User Data
					$row = $query_email->row();
					
					//Actualizamos la publicación
					$this->db->set('fbid', $response['id']);
					$this->db->set('access_token', $response['token']);
					$this->db->where('iduser', $row->iduser);
					$this->db->update('user');
					
					//Build User Array
					$data = array(
						'iduser' => $row->iduser,
						'email' => $row->email,
					    'name' => $row->name,
					    'lastname' => $row->lastname,
					    'code' => $row->code,
					    'verified' => $row->verified
					);
	
					//Save Admin in $_SESSION
					$this->session->set_userdata('user', $data);
					$this->session->set_userdata('ra_logged', true);
					
					//Redirect to Home
					redirect(base_url().'dashboard/');
				}
				else 
				{
					//Leemos el Código
					$invite = (isset($_SESSION['invite'])) ? (string)trim($_SESSION['invite']) : '';
					$this->session->set_userdata('invite', '');
					
					//Registramos al Usuario
					$data = array(
						'name' => $response['first_name'],
						'lastname' => $response['last_name'],
						'email' => $response['email'],
						'password' => sha1($response['id']),
						'fbid' => $response['id'],
						'access_token' => $response['token'],
						'createdAt' => date('Y-m-d H:i:s'),
						'invite' => $invite,
						'verified' => 0,
						'ip' => $ip,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Checamos si el registro fue con invitacion
					if ($invite)
					{
						//Consultamos los Puntos por invitaciones
						$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $invite . "' AND user.status = 1");
						
						//Verificamos que sea de las primeras 5 invitaciones
						if ($query_invitaciones->num_rows() < 5)
						{
							//Consultamos al dueño de la invitación
							$query_invite = $this->db->query("SELECT * FROM user WHERE user.code = '" . $invite . "' AND user.status = 1");
							$row_invite = $query_invite->row();
							
							//Registramos la invitación en el Log
							$data = array(
								'iduser' => $row_invite->iduser,
								'points' => '100',
								'action' => 'invite',
								'value' => $response['email'],
								'createdAt' => date('Y-m-d H:i:s'),
								'status' => 1
							);
							$this->db->insert('log', $data);
						}
					}
					
					//Código
					$code = sha1($iduser);
					$code = substr($code, 0, 8);
					
					//Creamos el código de invite
					$data = array(
						'code' => $code
					);
					$this->db->where('iduser', $iduser);
					$this->db->update('user', $data);
					
					//ENVIAMOS CORREO DE BIENVENIDA
					$name_array = explode(' ', $response['first_name']);
					$data['name'] = $name_array[0];
					$data['code'] = $code;
					
					//Correo de Notificación
					$subject = 'Bienvenida al #RetoActivia';
					$body = $this->load->view('mail/bienvenida', $data, TRUE);
					$altbody = strip_tags($body);
					$email = $response['email'];
			
					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
				        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
				        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email);
				        //$mail->AddBCC('milio.hernandez@gmail.com');
			
				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}
					
					//Creamos la SESSION
					$data = array(
						'iduser' => $iduser,
						'email' => $response['email'],
					    'name' => $response['first_name'],
					    'lastname' => $response['last_name'],
					    'code' => $code,
					    'verified' => 0
					);

					//Save Admin in $_SESSION
					$this->session->set_userdata('user', $data);
					$this->session->set_userdata('ra_logged', true);
					
					//Redirigimos a Gracias
					redirect( base_url() . 'gracias/facebook/' );
				}
			}
		}
		else
		{
			//Redirect to Home
			redirect(base_url());
		}
	}
}