<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avisodeprivacidad extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('general/privacy');
		$this->load->view('includes/footer');
	}
}
