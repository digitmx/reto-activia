<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}
	
	public function index()
	{
		//Facebook
		$data['facebook'] = $this->facebook->loginUrl();
			
		//Check Finish Promo
		if (date('Y-m-d H:i:s') < '2017-02-13 00:00:00')
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('general/home', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/finish', $data);
			$this->load->view('includes/footer');
		}
	}
	
	public function bienvenida()
	{
		//Load View
		$this->load->view('mail/bienvenida');
	}
}
