<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Leemos los Usuarios Registrados
		$query_all = $this->db->query("SELECT * FROM log WHERE log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-02-12 23:59:59' AND log.status = 1");
		$data['codes_all'] = $query_all->num_rows();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('codes/index', $data);
		$this->load->view('includes/footer');
	}
	
}