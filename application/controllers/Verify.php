<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}
	
	public function index($code = '')
	{
		//Verificamos el Codigo
		if ($code)
		{
			//Leemos el Código
			$query = $this->db->query("SELECT * FROM user WHERE code = '" . $code . "' AND status = 1 LIMIT 1");
			
			//Verificamos 
			if ($query->num_rows() > 0)
			{
				//Leemos el objeto
				$row = $query->row();
				
				//Verificamos que el Codigo leido corresponda a la session activa
				if ($row->code == $_SESSION['user']['code'])
				{
					//Leemos si el Usuario viene de Facebook
					if ($row->fbid) { $data['facebook'] = true; }
					else 
					{ $data['facebook'] = false; }
					
					//Verificamos el Usuario
					$update = array(
						'verified' => 1
					);
					$this->db->where('iduser', $row->iduser);
					$this->db->update('user', $update);
					
					//Reenviamos los datos del Usuario
					$data['user'] = $row;
					
					//Actualizamos la Session
					$user = array(
						'iduser' => $row->iduser,
						'email' => $row->email,
					    'name' => $row->name,
					    'lastname' => $row->lastname,
					    'code' => $row->code,
					    'verified' => $row->verified
					);

					//Save Admin in $_SESSION
					$this->session->set_userdata('user', $user);
					
					//Load Views
					$this->load->view('includes/header');
					$this->load->view('verify/index', $data);
					$this->load->view('includes/footer');
				}
				else
				{
					//Load Views
					$this->load->view('includes/header');
					$this->load->view('verify/match');
					$this->load->view('includes/footer');	
				}
			}
			else
			{
				//Load Views
				$this->load->view('includes/header');
				$this->load->view('verify/error');
				$this->load->view('includes/footer');
			}
		}
		else
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('verify/error');
			$this->load->view('includes/footer');
		}
	}
	
}