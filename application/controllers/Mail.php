<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		redirect( base_url() );
	}
	
	public function code()
	{
		//Load View
		$this->load->view('mail/code');
	}
	
	public function reset()
	{
		//Load View
		$this->load->view('mail/reset');
	}
	
	public function assign()
	{
		//Load View
		$this->load->view('mail/assign');
	}
}
