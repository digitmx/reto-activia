<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terminosycondiciones extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('general/terms');
		$this->load->view('includes/footer');
	}
}
