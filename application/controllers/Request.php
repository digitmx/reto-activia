<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;

		//Check Session
		//if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('verify/request');
		$this->load->view('includes/footer');
	}
	
}