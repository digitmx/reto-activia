<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;
		$verified = (isset($_SESSION['user']['verified'])) ? $_SESSION['user']['verified'] : 0;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
		else
		{
			if (!$verified) { redirect( base_url() . 'request/' ); }
		}
	}
	
	public function index()
	{
		//Leemos los Datos
		$data['iduser'] = (isset($_SESSION['user']['iduser'])) ? (string)trim($_SESSION['user']['iduser']) : '1';
		$data['email'] = (isset($_SESSION['user']['email'])) ? (string)trim($_SESSION['user']['email']) : 'contacto@retoactivia.mx';
		$name = (isset($_SESSION['user']['name'])) ? (string)trim($_SESSION['user']['name']) : 'María';
		$data['code'] = (isset($_SESSION['user']['code'])) ? (string)trim($_SESSION['user']['code']) : '356a192b';
		$name_array = explode(' ', $name);
		$data['name'] = $name_array['0'];
		$puntos_invitaciones = 0;
		$puntos_codigos = 0;
		
		//Consultamos los Puntos por invitaciones
		$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $data['code'] . "' AND status = 1");
		
		//Calculamos los Puntos por invitaciones
		if ($query_invitaciones->num_rows() > 5)
		{
			//Asignamos el monto máximo de invitaciones
			$puntos_invitaciones = 500;
		}
		else
		{
			//Calculamos los puntos por invitación
			$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
		}
		
		//Consultamos los Puntos por Códigos
		$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $data['iduser'] . " AND log.action = 'code' AND log.status = 1");
		
		//Procesamos los Puntos
		foreach ($query_puntos->result() as $row_puntos)
		{
			//Acumulamos
			$puntos_codigos = $puntos_codigos + $row_puntos->points;
		}
		
		//Data
		$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
		
		//Check Finish Promo
		if (date('Y-m-d H:i:s') < '2017-02-13 00:00:00')
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/index', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/finish', $data);
			$this->load->view('includes/footer');
		}
	}
}
