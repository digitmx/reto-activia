<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fails extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Read Session
		$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url().'users' ); }
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT fail.iduser, (SELECT name FROM user WHERE fail.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE fail.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE fail.iduser = user.iduser) as email, SUM(fail.points) as points FROM fail WHERE fail.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-01-22 23:59:59' AND fail.status = 1 GROUP BY fail.iduser ORDER BY points DESC");
		$data['uno'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT fail.iduser, (SELECT name FROM user WHERE fail.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE fail.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE fail.iduser = user.iduser) as email, SUM(fail.points) as points FROM fail WHERE fail.createdAt BETWEEN '2017-01-23 00:00:00' AND '2017-01-29 23:59:59' AND fail.status = 1 GROUP BY fail.iduser ORDER BY points DESC");
		$data['dos'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT fail.iduser, (SELECT name FROM user WHERE fail.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE fail.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE fail.iduser = user.iduser) as email, SUM(fail.points) as points FROM fail WHERE fail.createdAt BETWEEN '2017-01-30 00:00:00' AND '2017-02-05 23:59:59' AND fail.status = 1 GROUP BY fail.iduser ORDER BY points DESC");
		$data['tres'] = $query->result();
		
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT DISTINCT fail.iduser, (SELECT name FROM user WHERE fail.iduser = user.iduser) as name, (SELECT lastname FROM user WHERE fail.iduser = user.iduser) as lastname, (SELECT email FROM user WHERE fail.iduser = user.iduser) as email, SUM(fail.points) as points FROM fail WHERE fail.createdAt BETWEEN '2017-02-06 00:00:00' AND '2017-02-12 23:59:59' AND fail.status = 1 GROUP BY fail.iduser ORDER BY points DESC");
		$data['cuatro'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('fails/index', $data);
		$this->load->view('includes/footer');
	}
	
}