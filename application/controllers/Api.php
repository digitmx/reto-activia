<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Leemos los parametros
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		if (!$json) { $json = file_get_contents("php://input"); }
		
		//Comprobamos que el parametro no es NULO
		if ($json != NULL)
		{
			//Decodificamos los valores del JSON
			$json_decode = json_decode($json, true);

			//Leemos el Mensaje del JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';

			//Leemos los Campos del JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';

			//Leemos el Identificador de la App
			$app = (isset($json_decode['app'])) ? $json_decode['app'] : '';

			//Leemos el APIKEY de la App
			$apikey = (isset($json_decode['apikey'])) ? $json_decode['apikey'] : '';

			//Ejecutamos el JSON que mandaron
			$this->engine->executeJSON($msg,$fields,$app,$apikey);
		}
		else
		{
			//Redirect Home
			redirect( base_url() );
		}
	}
	
	public function sendDevolucion()
	{
		//Leemos los Datos
		$data['nombre'] = (isset($_POST['inputNombre'])) ? (string)trim($_POST['inputNombre']) : '';
		$data['apellidoPaterno'] = (isset($_POST['inputApellidoPaterno'])) ? (string)trim($_POST['inputApellidoPaterno']) : '';
		$data['telefono'] = (isset($_POST['inputTelefono'])) ? (string)trim($_POST['inputTelefono']) : '';
		$data['genero'] = (isset($_POST['inputGenero'])) ? (string)trim($_POST['inputGenero']) : '';
		$data['edad'] = (isset($_POST['inputEdad'])) ? (string)trim($_POST['inputEdad']) : '';
		$data['calle'] = (isset($_POST['inputCalle'])) ? (string)trim($_POST['inputCalle']) : '';
		$data['noExterior'] = (isset($_POST['inputNoExterior'])) ? (string)trim($_POST['inputNoExterior']) : '';
		$data['noInterior'] = (isset($_POST['inputNoInterior'])) ? (string)trim($_POST['inputNoInterior']) : '';
		$data['colonia'] = (isset($_POST['inputColonia'])) ? (string)trim($_POST['inputColonia']) : '';
		$data['delegacionMunicipio'] = (isset($_POST['inputDelegacionMunicipio'])) ? (string)trim($_POST['inputDelegacionMunicipio']) : '';
		$data['ciudad'] = (isset($_POST['inputCiudad'])) ? (string)trim($_POST['inputCiudad']) : '';
		$data['estado'] = (isset($_POST['inputEstado'])) ? (string)trim($_POST['inputEstado']) : '';
		$data['codigoPostal'] = (isset($_POST['inputCodigoPostal'])) ? (string)trim($_POST['inputCodigoPostal']) : '';
		$data['createdAt'] = date('Y-m-d H:i:s');
		$data['status'] = 1;
		$data['apellidoMaterno'] = (isset($_POST['materno'])) ? (string)trim($_POST['materno']) : '';
		$data['correoElectronico'] = (isset($_POST['email'])) ? (string)trim($_POST['email']) : '';
		
		//Guardamos el Registro
		$this->db->insert('devolucion', $data);	
		
		//Enviamos el Correo
		$subject = 'Contacto para Devolución Reto Activia';
		$body = $this->load->view('mail/devolucion', $data, TRUE);
		$altbody = strip_tags($body);

		//Verificamos
		if ($subject != '' && $body != '' && $altbody != '')
		{
			//Mandamos el Correo
			$mail = new PHPMailer(true);
			$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
			$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$mail->CharSet = 'UTF-8';
			$mail->Encoding = "quoted-printable";
	        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
	        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
	        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
	        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
	        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
	        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
	        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
	        $mail->SMTPDebug  = 0;
	        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
	        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
	        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
	        $mail->Body      = $body;
	        $mail->AltBody    = $altbody;
	        $mail->AddAddress('devoluciones@retoactivia.mx', 'Devoluciones Reto Activia');
	        $mail->AddBCC('contacto@retoactivia.mx', 'Contacto Reto Activia');
	        $mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

	        if ($mail->Send())
			{
				$response = true;
			}
			else
			{
				$response = false;
			}
		}
		
		//Regresamos el Status
		echo 'success';
	}
}