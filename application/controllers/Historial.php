<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['ra_logged'])) ? $_SESSION['ra_logged'] : false;
		$verified = (isset($_SESSION['user']['verified'])) ? $_SESSION['user']['verified'] : 0;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
		else
		{
			if (!$verified) { redirect( base_url() . 'verify/request/' ); }
		}
	}
	
	public function index()
	{
		//Leemos los Datos
		$data['iduser'] = (isset($_SESSION['user']['iduser'])) ? (string)trim($_SESSION['user']['iduser']) : '1';
		$data['email'] = (isset($_SESSION['user']['email'])) ? (string)trim($_SESSION['user']['email']) : 'contacto@retoactivia.mx';
		$name = (isset($_SESSION['user']['name'])) ? (string)trim($_SESSION['user']['name']) : 'María';
		$data['code'] = (isset($_SESSION['user']['code'])) ? (string)trim($_SESSION['user']['code']) : '356a192b';
		$name_array = explode(' ', $name);
		$data['name'] = $name_array['0'];
		
		//Paginación
		$page = $this->uri->segment(3,1);
		$limit = '10';
		$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
			
		//Consultamos el Log del Usuario
		$query_logs = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $data['iduser'] . "' AND log.status = 1 ORDER BY log.idlog DESC LIMIT " . $offset . "," . $limit);
		$data['logs'] = $query_logs->result();
		$query_all = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $data['iduser'] . "' AND log.status = 1");
		$data['logs_all'] = $query_all->result();
		$data['page'] = $page;
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('historial/index', $data);
		$this->load->view('includes/footer');
	}
}
