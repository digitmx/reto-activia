
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<div class="container">
						<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>close">Cerrar Sesión</a>
						<a class="gotham-book font16 white-text block underline codes" href="<?php echo base_url(); ?>fails">Códigos Erróneos</a>
						<a class="gotham-book font16 white-text block underline records" href="<?php echo base_url(); ?>users/dashboard">Registro de Códigos</a>
						<div class="space20"></div>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="space60"></div>
								<div class="right" style="margin-right: 10px;">
									<form id="formCodes" name="formCodes" accept-charset="utf-8" method="post">
										<div class="input-field col s12">
											<div class="inline">
												<select class="gotham-book" id="date" name="date">
													<option value="">Selecciona el día</option>
													<option value="2017-01-16">16 de Enero</option>
													<option value="2017-01-17">17 de Enero</option>
													<option value="2017-01-18">18 de Enero</option>
													<option value="2017-01-19">19 de Enero</option>
													<option value="2017-01-20">20 de Enero</option>
													<option value="2017-01-21">21 de Enero</option>
													<option value="2017-01-22">22 de Enero</option>
													<option value="2017-01-23">23 de Enero</option>
													<option value="2017-01-24">24 de Enero</option>
													<option value="2017-01-25">25 de Enero</option>
													<option value="2017-01-26">26 de Enero</option>
													<option value="2017-01-27">27 de Enero</option>
													<option value="2017-01-28">28 de Enero</option>
													<option value="2017-01-29">29 de Enero</option>
													<option value="2017-01-30">30 de Enero</option>
													<option value="2017-01-31">31 de Enero</option>
													<option value="2017-02-01">01 de Febrero</option>
													<option value="2017-02-02">02 de Febrero</option>
													<option value="2017-02-03">03 de Febrero</option>
													<option value="2017-02-04">04 de Febrero</option>
													<option value="2017-02-05">05 de Febrero</option>
													<option value="2017-02-06">06 de Febrero</option>
													<option value="2017-02-07">07 de Febrero</option>
													<option value="2017-02-08">08 de Febrero</option>
													<option value="2017-02-09">09 de Febrero</option>
													<option value="2017-02-10">10 de Febrero</option>
													<option value="2017-02-11">11 de Febrero</option>
													<option value="2017-02-12">12 de Febrero</option>
												</select>
											</div>
											<a class="waves-effect waves-light btn teak gotham-book text-normal border-white disabled" href="" id="btnXLS">Descargar XLS</a>
		    							</div>
									</form>
								</div>
								<span class="gotham-book font20 white-text">Lista de Códigos Válidos (<?php echo $codes_all; ?>)</span>
							</div>
						</div>
						<div class="row" id="results"></div>
					</div>
				</div>
			</div>
		</div>