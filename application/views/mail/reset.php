<?php $name = (isset($name)) ? (string)trim($name) : 'Claudia'; ?>
<?php $token = (isset($token)) ? (string)trim($token) : 'abcd1234'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Recuperar Contraseña</title>
      
      <style type="text/css">
         /* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
         body{ background-color: #FFF; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /* FONTS */
		@font-face { font-family: 'Gotham Black'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Black.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Black.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Black.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Black.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Black.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Black Italic'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BlackItalic.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BlackItalic.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BlackItalic.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BlackItalic.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BlackItalic.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Bold'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Bold.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Bold.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Bold.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Bold.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Bold.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Bold Italic'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BoldItalic.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BoldItalic.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BoldItalic.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BoldItalic.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BoldItalic.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Book'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Book.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Book.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Book.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Book.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Book.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Book Italic'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BookItalic.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BookItalic.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BookItalic.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BookItalic.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-BookItalic.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Light'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Light.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Light.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Light.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Light.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Light.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Light Italic'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-LightItalic.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-LightItalic.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-LightItalic.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-LightItalic.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-LightItalic.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		@font-face { font-family: 'Gotham Medium'; src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Medium.eot'); src: url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Medium.eot?#iefix') format('embedded-opentype'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Medium.woff2') format('woff2'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Medium.woff') format('woff'), url('https://retoactivia.mx/assets/fonts/Gotham/Gotham-Medium.svg#webfont') format('svg'); font-weight: normal; font-style: normal; }
		.gotham-black { font-family: 'Gotham Black', Helvetica, arial, sans-serif; }
		.gotham-black-italic { font-family: 'Gotham Black Italic', Helvetica, arial, sans-serif; }
		.gotham-bold { font-family: 'Gotham Bold', Helvetica, arial, sans-serif; }
		.gotham-bold-italic { font-family: 'Gotham Bold Italic', Helvetica, arial, sans-serif; }
		.gotham-book { font-family: 'Gotham Book', Helvetica, arial, sans-serif; }
		.gotham-book-italic { font-family: 'Gotham Book Italic', Helvetica, arial, sans-serif; }
		.gotham-light { font-family: 'Gotham Light', Helvetica, arial, sans-serif; }
		.gotham-light-italic { font-family: 'Gotham Light Italic', Helvetica, arial, sans-serif; }
		.gotham-medium { font-family: 'Gotham Medium', Helvetica, arial, sans-serif; }
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         /*img[class=banner] {width: 440px!important;height:220px!important;}*/
         img[class=colimg2] {width: 440px!important;height:220px!important;}
         
         
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #0a8cce !important; 
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         /*img[class=banner] {width: 280px!important;height:140px!important;}*/
         img[class=colimg2] {width: 280px!important;height:140px!important;}
         td[class=mobile-hide]{display:none!important;}
         td[class="padding-bottom25"]{padding-bottom:25px!important;}
        
         }
      </style>
   </head>
   <body>
	   
	   <div style="background-color:#0D4634;">
			<!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
		    <v:fill type="tile" src="https://retoactivia.mx/assets/img/back_activia.jpg" color="#0D4634"/>
			</v:background>
			<![endif]-->
			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
		    	<tr>
					<td valign="top" align="left" background="https://retoactivia.mx/assets/img/back_activia.jpg">
						<!-- Start of preheader -->
						<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" >
						   
						</table>
						<!-- End of preheader -->       
						<!-- Start of header -->
						<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td width="100%">
						                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						                           <tbody>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                              </tr>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td>
						                                    <!-- logo -->
						                                    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
						                                       <tbody>
						                                          <tr>
						                                             <td width="600" height="60" align="center">
						                                                <div class="imgpop"> <img src="https://retoactivia.mx/assets/img/header_mail.jpg" alt="" border="0" width="600" height="60" style="display:block; border:none; outline:none; text-decoration:none;">
						                                                </div>
						                                             </td>
						                                          </tr>
						                                       </tbody>
						                                    </table>
						                                    <!-- end of logo -->
						                                 </td>
						                              </tr>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                              </tr>
						                              <!-- Spacing -->
						                           </tbody>
						                        </table>
						                     </td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						<!-- End of Header -->
						<!-- Start of main-banner -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td width="100%">
						                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
						                           <tbody>
						                              <tr>
						                                 <!-- start of image -->
						                                 <td align="center" st-image="banner-image">
						                                    <div class="imgpop"> <img width="112" border="0" height="114" alt="" style="display:block; border:none; margin: 20px 0px; outline:none; text-decoration:none;" src="https://retoactivia.mx/assets/img/logo_activia_mail.png" class="banner">
						                                    </div>
						                                 </td>
						                              </tr>
						                           </tbody>
						                        </table>
						                        <!-- end of image -->
						                     </td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						<!-- End of main-banner --> 
						<!-- Start of seperator -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td align="center" height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						<!-- End of seperator -->   
						<!-- Start Full Text -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td width="100%">
						                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						                           <tbody>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                              </tr>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td>
						                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
						                                       <tbody>
						                                          <!-- Title -->
						                                          <tr>
						                                             <td class="gotham-bold" style="font-size: 30px; color: #FFF; text-align:center; line-height: 30px;" st-title="fulltext-heading">
						                                                Restablece tu Contraseña
						                                             </td>
						                                          </tr>
						                                          <!-- End of Title -->
						                                          <!-- spacing -->
						                                          <tr>
						                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                                          </tr>
						                                          <!-- End of spacing -->
						                                          <!-- content -->
						                                          <tr>
						                                             <td class="gotham-book" style="font-size: 16px; color: #FFF; text-align:center; line-height: 30px;" st-content="fulltext-content">
																	 Has solicitado cambiar de contraseña, para continuar con el proceso, haz click en el siguiente enlace.
																	 </td>
						                                          </tr>
						                                          <tr>
						                                            <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                                          </tr>
						                                          <tr>
						                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cd8;  text-align:center; line-height: 20px;" st-title="3col-readmore2" class="padding-bottom25"><span class="padding-bottom25" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cd8;  text-align:center; line-height: 20px;"> <a href="<?php echo base_url() . 'reset/token/'.$token; ?>" style="text-decoration:none; color: #F3DB7F; "><img height="37" width="279" style="height: 37px; width: 279px;" src="https://retoactivia.mx/assets/img/btn_cta_reset.jpg" /></a> </span></td>
						                                          </tr>
						                                          <!-- End of content -->
						                                       </tbody>
						                                    </table>
						                                 </td>
						                              </tr>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
						                              </tr>
						                              <!-- Spacing -->
						                           </tbody>
						                        </table>
						                     </td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						
						<!-- End of seperator --> 
						<!-- 2columns -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="2columns">
						   
						</table>
						
						<!-- end of full text -->
						<!-- Start of seperator -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
						                  </tr>
						                  <tr>
						                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
						                  </tr>
						                  <tr>
						                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						<!-- End of seperator -->  
						<!-- Start of Postfooter -->
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter" >
						   <tbody>
						      <tr>
						         <td>
						            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						               <tbody>
						                  <tr>
						                     <td width="100%">
						                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
						                           <tbody>
						                              <tr>
						                               	  <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #FFF; text-align:center; line-height: 30px;" st-content="fulltext-content">
														 	<span class="gotham-book" style="display: block; text-decoration: underline;"><a style="color: #FFF;" target="_blank" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a> · <a style="color: #FFF" target="_blank" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></span>
														 </td>
						                              </tr>
						                              <!-- Spacing -->
						                              <tr>
						                                 <td width="100%" height="20"></td>
						                              </tr>
						                              <!-- Spacing -->
						                           </tbody>
						                        </table>
						                     </td>
						                  </tr>
						               </tbody>
						            </table>
						         </td>
						      </tr>
						   </tbody>
						</table>
						<!-- End of postfooter -->
					</td>
				</tr>
			</table>
		</div>
   
   </body>
   </html>