<html>
	<body>
		<h4>Hola, hemos recibido la siguiente información sobre una devolución del Reto Activia.</h4>
		<hr>
		<h4>Datos Personales</h4>
		<p><b>Nombre:</b> <?php echo $nombre; ?></p>
		<p><b>Apellido Paterno:</b> <?php echo $apellidoPaterno; ?></p>
		<p><b>Apellido Materno:</b> <?php echo $apellidoMaterno; ?></p>
		<p><b>Correo Electrónico:</b> <?php echo $correoElectronico; ?></p>
		<p><b>Teléfono:</b> <?php echo $telefono; ?></p>
		<p><b>Género:</b> <?php echo $genero; ?></p>
		<p><b>Edad:</b> <?php echo $edad; ?></p>
		<h4>Dirección</h4>
		<p><b>Calle:</b> <?php echo $calle; ?></p>
		<p><b>No. Exterior:</b> <?php echo $noExterior; ?></p>
		<p><b>No. Interior:</b> <?php echo $noInterior; ?></p>
		<p><b>Colonia:</b> <?php echo $colonia; ?></p>
		<p><b>Delegación o Municipio:</b> <?php echo $delegacionMunicipio; ?></p>
		<p><b>Ciudad:</b> <?php echo $ciudad; ?></p>
		<p><b>Estado:</b> <?php echo $estado; ?></p>
		<p><b>Código Postal:</b> <?php echo $codigoPostal; ?></p>
	</body>
</html>