<?php
    $controller_slug = $this->uri->segment(1);
    if (!$controller_slug) { $controller_slug = 'home'; }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<meta name="google-site-verification" content="q8Dh8DnbyqLCXcjONu2SY7J6ilvntdAbWbvrlkCgWco" />
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="Reto Activia">
		<title>Reto Activia</title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!--<link rel="stylesheet" id="fontMaterialDesign-css" href="//fonts.googleapis.com/icon?family=Material+Icons" type="text/css" media="screen">
		<link rel="stylesheet" id="styles-css" href="<?php echo base_url(); ?>assets/css/materialize.min.css" type="text/css" media="screen">
		<link rel="stylesheet" id="time-css" href="<?php echo base_url(); ?>assets/css/jquery.timeentry.css" type="text/css" media="screen">
		<link rel="stylesheet" id="font-awesome-css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" type="text/css" media="screen">-->
		<link rel="stylesheet" id="styles-css" href="<?php echo base_url(); ?>assets/css/style.min.css" type="text/css" media="screen">
        <!-- Facebook Open Graph -->
        <meta property="fb:app_id" content="146199639197615" /> 
        <meta property="og:site_name" content="Reto Activia"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?php echo base_url(); ?>"/>
        <meta property="og:title" content="Yo ya soy parte de #RetoActivia ¡Únete!"/>
        <meta property="og:image" content="<?php echo base_url(); ?>assets/img/fb_placeholder.jpg"/>
        <meta property="og:description" content="Cuido de mi salud con Activia® y al mismo tiempo participo por increíbles premios."/>
        <!-- Twitter Open Graph -->
        <meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@activiamx" />
		<meta name="twitter:creator" content="@activiamx" />
		<meta property="twitter:title" content="Yo ya soy parte de #RetoActivia ¡Únete!" />
		<meta property="twitter:description" content="Cuido de mi salud con Activia® y al mismo tiempo participo por increíbles premios." />
		<meta property="twitter:image" content="<?php echo base_url(); ?>assets/img/tw_placeholder.jpg" />
        <script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>
		<!-- HTML5 shim and Respond.js IE8 support for HTML5 elements and media queries. -->
		<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/js/html5shiv-printshiv.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
        <![endif]-->
        <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-89583509-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
		<?php endif; ?>
	</head>
	<body class="<?php echo $controller_slug; ?>">
		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->