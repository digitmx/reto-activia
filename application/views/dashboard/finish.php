
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-bold font20 jumbo-text block">¡Muchas gracias por participar en el</span>
						<span class="gotham-bold font30 jumbo-text">#RetoActivia!</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid sherwood no-margin-row">
			<div class="row">
				<div class="col s12 m8 offset-m2 l4 offset-l4">
					<div class="space40"></div>
					<center>
						<span class="gotham-light font20 white-text">La promoción ha llegado a su fin, consulta a los ganadores en nuestras redes sociales.</span>
					</center>
					<div class="space20"></div>
					<center>
						<span class="gotham-bold font20 white-text">¡Ahora continua con los buenos hábitos!</span>
					</center>
					<div class="space20"></div>
					<center>
						<div class="logo_finish">
							<img src="<?php echo base_url(); ?>/assets/img/logo_activia.svg" />
						</div>
					</center>
					<div class="space20"></div>
					<center>
						<span class="gotham-book font16 white-text block underline"><a class="white-text" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a> · <a class="white-text" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></span>
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-bold font20 sherwood-text block">Síguenos</span>
						<div class="space20"></div>
						<a href="https://www.facebook.com/activiamexico" target="_blank" class="waves-effect waves-light btn yale gotham-bold text-normal border-white"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="https://twitter.com/activiamx" target="_blank" class="waves-effect waves-light btn bleu gotham-bold text-normal border-white"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</center>
					<div class="space40"></div>
				</div>
			</div>
		</div>
					