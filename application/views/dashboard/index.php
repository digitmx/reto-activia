
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="logo">
				<img src="<?php echo base_url(); ?>/assets/img/logo_activia.svg" />
			</div>
			<div class="row no-padding">
				<div class="col s12 m12 l6 sherwood" id="green-side">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l12">
							<div class="space40"></div>
							<div class="row">
								<div class="col s10 offset-s1 m10 offset-m1 l8 offset-l2">
									<center>
										<span class="gotham-book font20 white-text block">Registra tus códigos</span>
										<hr class="line" />
										<div class="space20"></div>
										<span class="gotham-bold font16 white-text block">INSTRUCCIONES</span>
										<span class="gotham-book font16 white-text block">Ingresa el código de tu Activia<br/>y suma puntos para ganar increíbles premios</span>
										<div class="space20"></div>
										<!--<span class="gotham-book-italic font10 white-text float-image">Este es<br/>tu código</span>-->
										<center>
											<img src="<?php echo base_url(); ?>assets/img/productos_final.png" class="responsive-img block" />
											<div class="space20 hide-on-large-only"></div>
											<img src="<?php echo base_url(); ?>assets/img/codigo.png" class="responsive-img block hide-on-large-only" />
										</center>
										<div class="space20"></div>
										<div class="row">
											<form class="col s12" id="formCode" name="formCode" accept-charset="utf-8" method="post">
												<input type="hidden" id="iduser" name="iduser" value="<?php echo $_SESSION['user']['iduser']; ?>" />
												<span class="gotham-bold font16 white-text block">Escribe tu código aquí.</span>
												<div class="row">
													<div class="input-field col s12">
														<select class="gotham-book" id="inputProducto" name="inputProducto">
															<option value="">Selecciona tu producto a registrar</option>
															<option value="bebible">Bebible (clásicos, cereales)</option>
															<option value="bebible">Bebible (0%)</option>
															<option value="semillas">Semillas (chía y quinoa)</option>
															<option value="cereales">Con cereales a un lado</option>
															<option value="kilo">Kilo</option>
															<option value="vasito">Vasito</option>
														</select>
													</div>
													<div class="input-field col s4">
														<input class="gotham-book" autocomplete="off" maxlength="7" placeholder="Fecha" id="inputDate" name="inputDate" type="text">
													</div>
													<div class="input-field col s4">
														<input class="gotham-book" autocomplete="off" maxlength="4" placeholder="Folio" id="inputFolio" name="inputFolio" type="text">
													</div>
													<div class="input-field col s4">
														<input class="gotham-book" autocomplete="off" maxlength="5" placeholder="HH:SS" id="inputHora" name="inputHora" type="text">
														<i class="fa fa-info-circle white-text info hide-on-med-and-down font20" aria-hidden="true"></i>
														<img src="<?php echo base_url(); ?>assets/img/codigo.png" class="info-text hide-on-med-and-down" />
													</div>
													<div class="input-field col s12">
														<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnCode">Añadir</a>
													</div>
												</div>
											</form>
										</div>
									</center>	
								</div>
							</div>
							<div class="row no-padding" style="margin-top: -40px;">								
								<div class="input-field col s12 teak">
									<span class="gotham-book block black-text padding-1rem centered">Recuerda que, en caso de que resultes ganador de algún premio, deberás comprobar la tenencia física de las tapas o botes de producto con el código que registraste.</span>
								</div>	
							</div>
							<div class="row no-padding">
								<div class="col s10 offset-s1 m10 offset-m1 l8 offset-l2">
									<center>							
										<div class="input-field col s12">
											<span class="gotham-book white-text"><span class="block" id="codigo_error">Este código ya fue utilizado, por favor ingresa un código nuevo. </span>¿Tienes problemas ingresando tu código? Ponte en contacto con nosotros a través del correo <a class="golden-text" href="mailto:contacto@retoactivia.mx">contacto@retoactivia.mx</a></span>
										</div>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6 lilac">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l8 offset-l2">
							<a class="gotham-book font16 sherwood-text block underline logout" href="<?php echo base_url(); ?>logout">Cerrar Sesión</a>
							<div class="space40"></div>
							<center>
								<span class="gotham-book font20 sherwood-text block">Hola <b><i><?php echo $name; ?></i></b>, llevas</span>
								<span class="gotham-book-italic font70 sherwood-text" id="dashboard_points"><?php echo $puntos; ?></span>
								<hr class="points">
								<span class="gotham-book font20 sherwood-text block">Puntos acumulados</span>
								<div class="space20"></div>
								<a class="gotham-book font20 sherwood-text block underline" href="<?php echo base_url(); ?>historial">Ver historial</a>
								<div class="space60"></div>
								<span class="gotham-bold font20 sherwood-text block">¿Quieres obtener más puntos?</span>
								<span class="gotham-book font16 sherwood-text block">Invita a tus amigos a participar con este enlace único que hicimos sólo para ti.</span>
								<div class="space20"></div>
								<span class="gotham-book font16 sherwood-text block">Por cada amigo que se registre en el <span class="gotham-bold">Reto Activia®</span>, a través de esta liga, conseguiras 100 puntos.</span>
								<span class="gotham-light-italic sherwood-text block">(Máximo 500 puntos).</span>
								<div class="space40"></div>
								<span class="gotham-bold font14 sherwood-text block">Está es tu liga</span>
								<span class="gotham-book font16 sherwood-text block underline" id="invite_code" data-clipboard-text="<?php echo base_url().'invite/'.$code; ?>"><?php echo base_url().'invite/'.$code; ?></span>
								<input id="code" type="hidden" value="<?php echo base_url().'invite/'.$code; ?>">
							</center>
							<div class="space100"></div>
							<center>
								<span class="gotham-book font16 sherwood-text block underline"><a class="sherwood-text" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a> · <a class="sherwood-text" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></span>
							</center>
						</div>
					</div>
				</div>
			</div>	
			<div class="row no-padding">
				<div class="col s12 m12 l12 white">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font16 sherwood-text block">Consulta las redes sociales de <b>Activia®</b> para enterarte de los premios semanales y los ganadores.</span>
						<div class="space20"></div>
						<span class="gotham-book font16 sherwood-text block">Comparte el reto</span>
						<div class="space20"></div>
						<a class="waves-effect waves-light btn yale gotham-bold text-normal border-white btnShareFacebook" rel="<?php echo $code; ?>"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Compartir</a>
						<a class="waves-effect waves-light btn bleu gotham-bold text-normal border-white btnShareTwitter" rel="<?php echo $code; ?>"><i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Compartir</a>
						<div class="space20"></div>
					</center>
				</div>
			</div>
		</div>