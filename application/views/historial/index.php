
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>logout">Cerrar Sesión</a>
					<div class="space40"></div>
					<div class="row">
						<div class="col s12 m10 offset-m1 l8 offset-l2">
							<center>
								<table class="centered responsive-table" id="table_historial">
									<tbody>
										<?php foreach ($logs as $log) { ?>
										<tr>
											<td class="gotham-book white-text">
												<?php 
													switch ($log->action) {
														case 'invite': echo 'Share Invitación'; break;
														case 'code': echo 'Código Registrado'; break;
														case 'assign': echo 'Código Asignado'; break;
													}
												?>
											</td>
											<td class="gotham-book white-text"><?php echo $log->value; ?></td>
											<td class="gotham-bold white-text"><?php echo $log->points; ?> pts</td>
			    						</tr>
			    						<?php } ?>
									</tbody>
								</table>
								<!--
								<ul class="pagination">
								    <li class="<?=($page == '1') ? 'disabled' : ''; ?>"><a href="<?php echo base_url(); ?>historial/index/<?=($page == '1') ? $page : (int)$page-1; ?>"><i class="material-icons">chevron_left</i></a></li>
								    <?php $division = $all_logs / 2; ?>
								    <?php if ($division <= 1) { ?>
								    <li class="active"><a href="#!">1</a></li>
								    <?php } else { echo $all_logs % 2; ?>
								    <li class="waves-effect"><a href="#!">2</a></li>
								    <li class="waves-effect"><a href="#!">3</a></li>
								    <li class="waves-effect"><a href="#!">4</a></li>
								    <li class="waves-effect"><a href="#!">5</a></li>
								    <?php } ?>
								    <li class="waves-effect"><a href="<?php echo base_url(); ?>historial/index/<?=($page == '1') ? $page : (int)$page+1; ?>"><i class="material-icons">chevron_right</i></a></li>
								</ul>
								-->
								<div class="space20"></div>
								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php base_url(); ?>dashboard">Regresar</a>
								<div class="space40"></div>
								<span class="gotham-book font16 white-text block underline"><a class="white-text" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a> · <a class="white-text" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></span>
								<div class="space20"></div>
							</center>
						</div>
					</div>
					<div class="row" id="pagination">
						<?php if ($page > 1) { ?>
						<div class="left">
							<div class="space20"></div>
							<a href="<?php echo base_url(); ?>historial/index/<?php echo (int)$page-1; ?>" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
						</div>
						<?php } ?>
						<?php $contador = (int)$page * 10; ?>
						<?php if ($contador < count($logs_all)) { ?> 
						<div class="right">
							<div class="space20"></div>
							<a href="<?php echo base_url(); ?>historial/index/<?php echo (int)$page+1; ?>" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>