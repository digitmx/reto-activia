
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space40"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space40"></div>
				</div>
			</div>
		</div>
		<div class="container white">
			<div class="row">
				<div class="col s12">
					<span class="gotham-bold font30 sherwood-text block">POLITICAS DE PRIVACIDAD</span>
					<span class="gotham-bold font20 sherwood-text block">POLÍTICAS DE PRIVACIDAD DE GRUPO DANONE</span>
					<p class="gotham-book font16 sherwood-text">Con el fin de informarle sobre las políticas y actividades en relación con la obtención, uso y transferencia de sus Datos Personales en sitios de Internet o de forma física, Danone de México, S.A. de C.V., Bonafont, S.A. de C.V., Liquimex, S.A. de C.V., Organización de Aguas de México, S.A. de C.V., Organización FR México, S.A. de C.V., Danone Medical  Nutrition México, S.A. de C.V., Danone Baby Nutrition México, S.A. de C.V., Instituto Danone, A.C., Envasadoras La Suprema, S.A. de C.V., Envasadoras de Aguas en México, S. de R.L. de C.V., Nomiser, S.A. de C.V., Compañía General de Aguas S. de R.L. de C.V., Aguas Embotelladas Los Pinos, S. de R.L. de C.V., Aguas Purificadas, Naturales y Minerales, S. de R.L. de C.V., Envasadoras Aguabon S. de R.L. de C.V., Envasabon, S. de R.L. de C.V., y/o de cualquier afiliada, subsidiaria del Grupo de empresas arriba mencionadas (conjuntamente referidos aquí como “Danone”, “Grupo Danone” o “Nosotros”) está consciente de sus obligaciones ante la Ley Federal de Protección de Datos Personales en Posesión de Particulares (la “Ley”), su reglamento y demás lineamientos que conjuntamente integran la regulación de protección de datos personales, y considera importante salvaguardar la privacidad personal, por lo que pone a su disposición esta Política de Privacidad (la “Política” o la “Política de Privacidad”). Esta Política de Privacidad detalla la manera en la que Danone y sus funcionarios, representantes, empleados, proveedores, así como otras entidades que actúan en representación de, en coparticipación, como aliados estratégicos y/o con la autorización de Danone, para la obtención, protección, uso y transferencia de su información recabada en nuestra página web (“Página Web”), mediante correo electrónico o de manera física o presencial, con estricto apego a lo establecido en el Aviso de Privacidad respectivo, así como a la presente Política de Privacidad.</p>
					<p class="gotham-book font16 sherwood-text">Al hacer uso de la Página Web, otorgas tu consentimiento para que Danone procese tus datos personales y/o los transfiera a sus subsidiarias y/o afiliadas, así como a las personas que Danone considere competentes para procesar la información aquí recabada, dentro del margen de los fines descritos más adelante.</p>
					<span class="gotham-bold font16 sherwood-text block">1. OBTENCIÓN DE INFORMACIÓN (INCLUYENDO DATOS PERSONALES)</span>
					<p class="gotham-book font16 sherwood-text">A pesar de que no es necesario proporcionar Datos Personales para ingresar a la Página Web, para utilizar ciertas funciones de nuestros sitios de internet, recabamos cierta información no-personal de manera automática, (incluyendo pero sin limitar a direcciones IP, clase de buscador, sitios web de referencia, cookies y horarios de acceso), sin que pueda esta información volverlo identificable (de conformidad con los términos establecidos en la Ley). La información antes mencionada es recabada para fines estadísticos de Danone.</p>
					<p class="gotham-book font16 sherwood-text">La entrega de sus Datos Personales a Danone es voluntaria, y en muchos casos necesaria para la entera funcionalidad de todos los servicios que provee nuestra Página Web. En caso de que usted no proporcione dichos Datos Personales, Danone se encontrará imposibilitado de proveer el servicio entero de la Página Web. Entre los datos recabados están: nombre, dirección, fecha de nacimiento, correo electrónico, número de teléfono, y género.</p>
					<p class="gotham-book font16 sherwood-text">Las finalidades con las que son recabados sus Datos Personales son las siguientes: 
						<ul>
							<li class="gotham-book font16 sherwood-text">A.    Promoción Directa</li>
							<li class="gotham-book font16 sherwood-text">B.    Mercadotecnia</li>
							<li class="gotham-book font16 sherwood-text">C.    Reclutamiento</li>
							<li class="gotham-book font16 sherwood-text">D.    Conducción de encuestas</li>
							<li class="gotham-book font16 sherwood-text">E.    Estadística</li>
							<li class="gotham-book font16 sherwood-text">F.    Discusión</li>
							<li class="gotham-book font16 sherwood-text">G.    Optimización</li>
						</ul>
					</p>
					<p class="gotham-book font16 sherwood-text">De igual forma, podremos solicitar, de manera voluntaria para los visitantes de nuestra Página Web, sus opiniones respecto a nuestros productos, servicios y Página Web, así como de sus preferencias, gustos, y demás información que nos permitan proveer un mejor servicio y producto. Esta información será utilizada de manera exclusiva para fines estadísticos, de mercadotecnia y estratégicos para ofrecer mejores servicios y productos.</p>
					<p class="gotham-book font16 sherwood-text">Danone podrá utilizar distintos sistemas, servicios y/o productos para procesar la información obtenida del uso de la Página Web.</p>
					<span class="gotham-bold font16 sherwood-text block">1.1. USO DE COOKIES Y OTRAS TECNOLOGÍAS</span>
					<p class="gotham-book font16 sherwood-text">La Página Web utiliza cookies, mismos que podrán ser negados por ti como visitante de la Página Web mediante los ajustes correspondientes al explorador. El uso de cookies es indispensable para el funcionamiento completo de la Página Web y su contenido, mejorando la experiencia del usuario. Estas cookies pueden ser almacenadas en el dispositivo que se esté usando para visitar la Página Web.</p>
					<p class="gotham-book font16 sherwood-text">Es posible que Nosotros nos veamos en la necesidad de solicitar Datos Personales Sensibles para las finalidades que en su caso se indiquen en el aviso de privacidad respectivo, situación que les será informada a efecto de obtener su consentimiento de conformidad con la Ley.</p>
					<p class="gotham-book font16 sherwood-text">Danone podrá utilizar otras tecnologías que funcionen a la par de, o que tengan funciones similares a las cookies de las cuales obtenga información de los visitantes de la Página Web, mismas que podrán estar incluidas en mensajes o boletines, a efecto de determinar si los mensajes fueron abiertos, tales como web beacons o similares.</p>
					<span class="gotham-bold font16 sherwood-text block">2. USO DE DATOS PERSONALES OBTENIDAS EN LA PÁGINA WEB</span>
					<p class="gotham-book font16 sherwood-text">La información proporcionada a Danone se utilizará para los fines arriba señalados, y no se comercializará ni se transferirá a terceros diferentes a los arriba señalados, o a los fines establecidos en los avisos de privacidad que lleguen a tener lugar.</p>
					<p class="gotham-book font16 sherwood-text">Danone, o los terceros que reciban la información de conformidad con esta Política, podrán utilizar y procesar tanto los Datos Personales, como la información que no haga identificable al usuario de la Página Web, de forma individual o colectiva para los fines arriba señalados.</p> 
					<span class="gotham-bold font16 sherwood-text block">3. TRANSFERENCIA DE DATOS PERSONALES</span>
					<p class="gotham-book font16 sherwood-text">Salvo por que sea dispuesto de manera distinta en la presente Política o en el aviso de privacidad respectivo, los Datos Personales recabados por Danone se utilizarán para los fines para los que fueron proporcionados, y serán transferidos únicamente a las empresas filiales o subsidiarias de Danone y cada una de las áreas que las integran, así como a las empresas que Danone contrate para procesar y analizar dicha información. El receptor de su información, será solicitado por Nosotros de tratar los Datos Personales con los mismos niveles de seguridad que con los que nosotros los tratamos.</p>
					<span class="gotham-bold font16 sherwood-text block">4. DATOS PERSONALES DE MENORES DE EDAD</span>
					<p class="gotham-book font16 sherwood-text">Para el caso de la obtención de Datos Personales de menores de edad, se deberá de contar con el consentimiento del padre, madre o tutor, y se deberá de proporcionar copia de la documentación que acredite dicha relación (incluyendo sin limitar, acta de nacimiento e identificaciones).</p>
					<span class="gotham-bold font16 sherwood-text block">5. MEDIDAS DE SEGURIDAD</span>
					<p class="gotham-book font16 sherwood-text">Danone protege y continuará protegiendo sus Datos Personales con todos los esfuerzos razonables para asegurarse aquellos recabados en la Página Web o de cualquier otra manera, cumplen con lo dispuesto en la Ley. No obstante lo anterior, sugerimos que la persona que visita la Página Web también cuente con medidas adicionales para proteger su información personal. Danone no se hace responsable por el acceso del visitante a la Página Web a links de terceros de cuyas políticas de privacidad Danone no tenga control.</p>
					<span class="gotham-bold font16 sherwood-text block">6. REVISIÓN Y ACTUALIZACIÓN DE LA INFORMACIÓN RECABADA</span>
					<p class="gotham-book font16 sherwood-text">Los visitantes de la Página Web que provean Datos Personales a Danone, podrán ejercer sus derechos de acceso, rectificación, cancelación y oposición, de conformidad con la Ley en cualquier momento contactando al área legal de Grupo Danone.</p>
					<span class="gotham-bold font16 sherwood-text block">7. ACTUALIZACIONES A ESTA POLÍTICA DE PRIVACIDAD</span>
					<p class="gotham-book font16 sherwood-text">Danone se reserva el derecho de actualizar, ajustar, y modificar estas Políticas en cualquier momento y sin previo aviso. De igual manera, Danone podremos cambiar los productos, políticas, promociones y/o fines de esta Página Web sin previo aviso. Lo invitamos a revisar los cambios a esta Política de manera regular.</p>
					<span class="gotham-bold font16 sherwood-text block">8. CONTACTO</span>
					<p class="gotham-book font16 sherwood-text">Cualquier duda o comentario a las presentes Políticas puede ser atendido por el área Legal o Compliance de Grupo Danone, o enviar un correo a Compliance.MX@danone.com</p>
					<p class="gotham-book font16 sherwood-text">Esta política de privacidad ha sido actualizada en Diciembre 2016.</p>
					<p class="gotham-book font16 sherwood-text">Lea más en http://www.activia.com.mx/content/politicas-de-privacidad#HtFsVbY1R64gTQDe.99</p>
				</div>
			</div>
			<div class="row">
				<div class="col s12 centered">
					<a href="<?php echo base_url(); ?>" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Regresar</a>
					<div class="space40"></div>
				</div>
			</div>
		</div>