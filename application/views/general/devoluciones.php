
		<div class="container-fluid">
			<div class="row">
				<form id="formDevolucion" name="formDevolucion" class="col s12 m10 offset-m1 l6 offset-l3">
					<div class="space20"></div>
					<span class="gotham-bold font20 sherwood-text block centered">Formato de Devoluciones</span>
					<div class="space20"></div>
					<div class="row">
						<legend class="gotham-book red-text invalid centered">Campo(s) obligatorio(s).</legend>
						<legend class="gotham-book sherwood-text">Datos Personales</legend>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Nombre (s)" id="inputNombre" name="inputNombre" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Apellido Paterno" id="inputApellidoPaterno" name="inputApellidoPaterno" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Apellido Materno" id="inputApellidoMaterno" name="inputApellidoMaterno" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Correo Electrónico" id="inputCorreoElectronico" name="inputCorreoElectronico" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Teléfono" id="inputTelefono" name="inputTelefono" type="text">
						</div>
						<div class="input-field col s12">
							<select class="gotham-book browser-default" id="inputGenero" name="inputGenero">
								<option value="">Selecciona un Género</option>
								<option value="Masculino">Masculino</option>
								<option value="Femenino">Femenino</option>
							</select>
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Edad (número en años)" id="inputEdad" name="inputEdad" type="number">
						</div>
					</div>
					<div class="row">
						<legend class="gotham-book sherwood-text">Dirección</legend>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Calle" id="inputCalle" name="inputCalle" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="No. Exterior" id="inputNoExterior" name="inputNoExterior" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="No. Interior" id="inputNoInterior" name="inputNoInterior" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Colonia" id="inputColonia" name="inputColonia" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Delegación o Municipio" id="inputDelegacionMunicipio" name="inputDelegacionMunicipio" type="text">
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Ciudad" id="inputCiudad" name="inputCiudad" type="text">
						</div>
						<div class="input-field col s12">
							<select class="gotham-book browser-default" id="inputEstado" name="inputEstado">
								<option value="">Selecciona un Estado</option>
								<option value="Aguascalientes">Aguascalientes</option>
								<option value="Baja California">Baja California</option>
								<option value="Baja California Sur">Baja California Sur</option>
								<option value="Campeche">Campeche</option>
								<option value="Chiapas">Chiapas</option>
								<option value="Chihuahua">Chihuahua</option>
								<option value="Ciudad de México">Ciudad de México</option>
								<option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
								<option value="Colima">Colima</option>
								<option value="Durango">Durango</option>
								<option value="Estado de México">Estado de México</option>
								<option value="Guanajuato">Guanajuato</option>
								<option value="Guerrero">Guerrero</option>
								<option value="Hidalgo">Hidalgo</option>
								<option value="Jalisco">Jalisco</option>
								<option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
								<option value="Morelos">Morelos</option>
								<option value="Nayarit">Nayarit</option>
								<option value="Nuevo León">Nuevo León</option>
								<option value="Oaxaca">Oaxaca</option>
								<option value="Puebla">Puebla</option>
								<option value="Querétaro">Querétaro</option>
								<option value="Quintana Roo">Quintana Roo</option>
								<option value="San Luis Potosí">San Luis Potosí</option>
								<option value="Sinaloa">Sinaloa</option>
								<option value="Sonora">Sonora</option>
								<option value="Tabasco">Tabasco</option>
								<option value="Tamaulipas">Tamaulipas</option>
								<option value="Tlaxcala">Tlaxcala</option>
								<option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la Llave</option>
								<option value="Yucatán">Yucatán</option>
								<option value="Zacatecas">Zacatecas</option>
							</select>
						</div>
						<div class="input-field col s12">
							<input class="gotham-book" autocomplete="off" placeholder="Código Postal" id="inputCodigoPostal" name="inputCodigoPostal" maxlength="5" type="number">
						</div>
						<div class="input-field col s12">
							<center>
								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnEnviarDevolucion">Enviar</a>
							</center>
						</div>
					</div>
					<div class="space20"></div>
				</form>
			</div>
			<div class="row">
				<div class="col s12 centered">
					<span class="gotham-book font16 sherwood-text block underline"><a class="sherwood-text" href="#terms">Términos y Condiciones</a> · <a class="sherwood-text" href="#privacy">Aviso de Privacidad</a></span>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<!-- Modal Structure -->
		<div id="terms" class="modal modal-fixed-footer">
			<div class="modal-content">
				<span class="gotham-bold font20 sherwood-text block centered">BASES LEGALES DE LA CAMPAÑA “RETO ACTIVIA®”</span>
				<p class="gotham-book font16 sherwood-text">VIGENCIA: 09 DE ENERO AL 28 DE FEBRERO DEL 2017.</p>
				<p class="gotham-book font16 sherwood-text">Mecánica de participación: Es necesario unirse al Reto Activia®, consumiendo 2 porciones de 125grs. por 4 semanas, acompañado de ejercicio, dieta y un estilo de vida saludable.</p>
				<p class="gotham-book font16 sherwood-text">TÉRMINOS Y CONDICIONES:</p>
				<p class="gotham-book font16 sherwood-text">Únete al Reto Activia®, consumiendo 2 porciones de 125grs. por 4 semanas, acompañado de ejercicio, dieta y un estilo de vida saludable y si no estás conforme con el sabor o consistencia del producto, te devolvemos tu dinero.</p>
				<p class="gotham-book font16 sherwood-text">Para que se le devuelva el dinero al consumidor, éste tendrá que enviar cualquier combinación de tapas de productos en buen estado (en su caso se deberá poder distinguir los datos ahí impresos) que juntos sumen 7,200 gramos de ACTIVIA® o más, así como copia de una identificación oficial (credencial de elector o pasaporte), escrito libre con la siguiente información: Nombre completo del consumidor a solicitar la devolución de su dinero; Dirección completa (calle y número, colonia, delegación o municipio, C.P. y teléfono, y la descripción de cómo consumió ACTIVIA® y el por qué no está conforme con el sabor o consistencia del producto; firma del consumidor que solicita la devolución de su dinero, como aparece en su identificación oficial.</p>
				<p class="gotham-book font16 sherwood-text">Asimismo en el escrito libre, deberá poner la información bancaria para su reembolso (nombre del titular, banco número de cuenta, número de clabe, número de sucursal, ya sea cuenta de cheques, de ahorro, tarjeta de débito o tarjeta de crédito).</p>
				<p class="gotham-book font16 sherwood-text">Para reclamar su reembolso deberá ingresar a la página www.activia.com.mx/retoactivia/devoluciones/formulario  en donde debe llenar un formulario con sus datos (Nombre completo, teléfono, género, edad, domicilio -calle, número, colonia, delegación o municipio, ciudad, estado, cp- y correo electrónico), al hacerlo recibirá un correo con las indicaciones para enviar los requisitos listados anteriormente y las instrucciones para obtener el reembolso de su dinero, en cuanto se reciba su sobre se verificarán todos los documentos y requisitos.</p>
				<p class="gotham-book font16 sherwood-text">En caso de estar completa toda la documentación arriba mencionada se realizará el reembolso a la cuenta de banco que informó el participante, en caso contrario nos comunicaremos para solicitarle nos envié los requisitos o la documentación faltante, para poder así realizar el reembolso.</p>
				<p class="gotham-book font16 sherwood-text">Posterior al reembolso realizado, nos comunicaremos con el consumidor para confirmar la recepción del dinero. En caso de que el consumidor haya proporcionado información errónea, Danone de México, S.A. de C.V. no se hará responsable por la entrega del dinero a persona distinta del participante, o en su defecto al no proporcionar los datos correctos de contacto perderá la oportunidad de poder recibir el reembolso. Lo anterior sin excepción de que Danone o cualquiera de sus empresas hermanas en México o en el extranjero inicien el procedimiento judicial o administrativo respectivo con las autoridades competentes.</p>
				<p class="gotham-book font16 sherwood-text">El reembolso se encuentra limitado al equivalente de $300.00 (trescientos pesos 00/100 M.N.) por consumidor, dirección y/o cuenta bancaria.</p>
				<p class="gotham-book font16 sherwood-text">Los reembolsos se realizarán conforme se reciban las cartas en el apartado postal, una vez recibida, en 48 horas después se realizará el reembolso a la cuenta bancaria proporcionada.</p>
				<p class="gotham-book font16 sherwood-text">No hay límite de número de devoluciones, pero estará sujeto a un máximo de reembolso de $300.00 (trescientos pesos 00/100 M.N.) por consumidor, dirección y/o cuenta bancaria; vigente del 09 de enero de 2017 al 28 de febrero de 2017 válido en toda la república mexicana, el consumo diario mínimo recomendado para participar en el reto ACTIVIA® es de 240grs o 2 porciones de 125g durante 28 días dentro de un estilo de vida y dieta saludable. Aplican restricciones.</p>
				<p class="gotham-book font16 sherwood-text">Danone de México, S.A. de .C.V, se reserva el derecho de limitar o prohibir la participación en esta promoción, a cualquier persona, en caso de presumir la existencia de fraude o alteración en la mecánica de funcionamiento del concurso, o si el participante no cumple en su totalidad con los requisitos de participación establecidos en las presentes bases.</p>
				<p class="gotham-book font16 sherwood-text">Danone de México, S.A. de .C.V sus subsidiarias y/o afiliadas, no serán responsables por cualquier incumplimiento relacionado con la promoción, derivado de caso fortuito o fuerza mayor o por cualquier otra causa fuera de su control, incluyendo: (i) condiciones climáticas adversas; (ii) incendio; (iii) inundación; (iv) huracán; (v) terremoto; (vi) movimiento de insurrección; (vii) conmoción civil; (viii) accidentes inevitables; (ix) legislación superveniente (x) acontecimientos imprevistos dentro de cada una de las tiendas participantes o cualquier otra circunstancia.</p>
				<p class="gotham-book font16 sherwood-text">La participación en la promoción implica el conocimiento y aceptación de las presentes bases, mecánica, términos y condiciones aquí establecidas.</p>
				<p class="gotham-book font16 sherwood-text">Responsable de la promoción DANONE DE MÉXICO, S.A. de C.V., ubicado en Mario Pani No. 400, Col. Santa Fe Cuajimalpa, Del. Cuajimalpa de Morelos, C.P. 05348; para cualquier aclaración comunicarse de lunes a viernes de 9 am a 6pm al teléfono: 01800-7024000 en el D.F y para el interior de la República.</p>
			</div>
			<div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect waves-light btn teak gotham-book text-normal border-white">Cerrar</a>
			</div>
		</div>
		<!-- Modal Structure -->
		<div id="privacy" class="modal modal-fixed-footer">
			<div class="modal-content">
				<span class="gotham-bold font30 sherwood-text block">POLITICAS DE PRIVACIDAD</span>
				<span class="gotham-bold font20 sherwood-text block">POLÍTICAS DE PRIVACIDAD DE GRUPO DANONE</span>
				<p class="gotham-book font16 sherwood-text">Con el fin de informarle sobre las políticas y actividades en relación con la obtención, uso y transferencia de sus Datos Personales en sitios de Internet o de forma física, Danone de México, S.A. de C.V., Bonafont, S.A. de C.V., Liquimex, S.A. de C.V., Organización de Aguas de México, S.A. de C.V., Organización FR México, S.A. de C.V., Danone Medical  Nutrition México, S.A. de C.V., Danone Baby Nutrition México, S.A. de C.V., Instituto Danone, A.C., Envasadoras La Suprema, S.A. de C.V., Envasadoras de Aguas en México, S. de R.L. de C.V., Nomiser, S.A. de C.V., Compañía General de Aguas S. de R.L. de C.V., Aguas Embotelladas Los Pinos, S. de R.L. de C.V., Aguas Purificadas, Naturales y Minerales, S. de R.L. de C.V., Envasadoras Aguabon S. de R.L. de C.V., Envasabon, S. de R.L. de C.V., y/o de cualquier afiliada, subsidiaria del Grupo de empresas arriba mencionadas (conjuntamente referidos aquí como “Danone”, “Grupo Danone” o “Nosotros”) está consciente de sus obligaciones ante la Ley Federal de Protección de Datos Personales en Posesión de Particulares (la “Ley”), su reglamento y demás lineamientos que conjuntamente integran la regulación de protección de datos personales, y considera importante salvaguardar la privacidad personal, por lo que pone a su disposición esta Política de Privacidad (la “Política” o la “Política de Privacidad”). Esta Política de Privacidad detalla la manera en la que Danone y sus funcionarios, representantes, empleados, proveedores, así como otras entidades que actúan en representación de, en coparticipación, como aliados estratégicos y/o con la autorización de Danone, para la obtención, protección, uso y transferencia de su información recabada en nuestra página web (“Página Web”), mediante correo electrónico o de manera física o presencial, con estricto apego a lo establecido en el Aviso de Privacidad respectivo, así como a la presente Política de Privacidad.</p>
				<p class="gotham-book font16 sherwood-text">Al hacer uso de la Página Web, otorgas tu consentimiento para que Danone procese tus datos personales y/o los transfiera a sus subsidiarias y/o afiliadas, así como a las personas que Danone considere competentes para procesar la información aquí recabada, dentro del margen de los fines descritos más adelante.</p>
				<span class="gotham-bold font16 sherwood-text block">1. OBTENCIÓN DE INFORMACIÓN (INCLUYENDO DATOS PERSONALES)</span>
				<p class="gotham-book font16 sherwood-text">A pesar de que no es necesario proporcionar Datos Personales para ingresar a la Página Web, para utilizar ciertas funciones de nuestros sitios de internet, recabamos cierta información no-personal de manera automática, (incluyendo pero sin limitar a direcciones IP, clase de buscador, sitios web de referencia, cookies y horarios de acceso), sin que pueda esta información volverlo identificable (de conformidad con los términos establecidos en la Ley). La información antes mencionada es recabada para fines estadísticos de Danone.</p>
				<p class="gotham-book font16 sherwood-text">La entrega de sus Datos Personales a Danone es voluntaria, y en muchos casos necesaria para la entera funcionalidad de todos los servicios que provee nuestra Página Web. En caso de que usted no proporcione dichos Datos Personales, Danone se encontrará imposibilitado de proveer el servicio entero de la Página Web. Entre los datos recabados están: nombre, dirección, fecha de nacimiento, correo electrónico, número de teléfono, y género.</p>
				<p class="gotham-book font16 sherwood-text">Las finalidades con las que son recabados sus Datos Personales son las siguientes: 
					<ul>
						<li class="gotham-book font16 sherwood-text">A.    Promoción Directa</li>
						<li class="gotham-book font16 sherwood-text">B.    Mercadotecnia</li>
						<li class="gotham-book font16 sherwood-text">C.    Reclutamiento</li>
						<li class="gotham-book font16 sherwood-text">D.    Conducción de encuestas</li>
						<li class="gotham-book font16 sherwood-text">E.    Estadística</li>
						<li class="gotham-book font16 sherwood-text">F.    Discusión</li>
						<li class="gotham-book font16 sherwood-text">G.    Optimización</li>
					</ul>
				</p>
				<p class="gotham-book font16 sherwood-text">De igual forma, podremos solicitar, de manera voluntaria para los visitantes de nuestra Página Web, sus opiniones respecto a nuestros productos, servicios y Página Web, así como de sus preferencias, gustos, y demás información que nos permitan proveer un mejor servicio y producto. Esta información será utilizada de manera exclusiva para fines estadísticos, de mercadotecnia y estratégicos para ofrecer mejores servicios y productos.</p>
				<p class="gotham-book font16 sherwood-text">Danone podrá utilizar distintos sistemas, servicios y/o productos para procesar la información obtenida del uso de la Página Web.</p>
				<span class="gotham-bold font16 sherwood-text block">1.1. USO DE COOKIES Y OTRAS TECNOLOGÍAS</span>
				<p class="gotham-book font16 sherwood-text">La Página Web utiliza cookies, mismos que podrán ser negados por ti como visitante de la Página Web mediante los ajustes correspondientes al explorador. El uso de cookies es indispensable para el funcionamiento completo de la Página Web y su contenido, mejorando la experiencia del usuario. Estas cookies pueden ser almacenadas en el dispositivo que se esté usando para visitar la Página Web.</p>
				<p class="gotham-book font16 sherwood-text">Es posible que Nosotros nos veamos en la necesidad de solicitar Datos Personales Sensibles para las finalidades que en su caso se indiquen en el aviso de privacidad respectivo, situación que les será informada a efecto de obtener su consentimiento de conformidad con la Ley.</p>
				<p class="gotham-book font16 sherwood-text">Danone podrá utilizar otras tecnologías que funcionen a la par de, o que tengan funciones similares a las cookies de las cuales obtenga información de los visitantes de la Página Web, mismas que podrán estar incluidas en mensajes o boletines, a efecto de determinar si los mensajes fueron abiertos, tales como web beacons o similares.</p>
				<span class="gotham-bold font16 sherwood-text block">2. USO DE DATOS PERSONALES OBTENIDAS EN LA PÁGINA WEB</span>
				<p class="gotham-book font16 sherwood-text">La información proporcionada a Danone se utilizará para los fines arriba señalados, y no se comercializará ni se transferirá a terceros diferentes a los arriba señalados, o a los fines establecidos en los avisos de privacidad que lleguen a tener lugar.</p>
				<p class="gotham-book font16 sherwood-text">Danone, o los terceros que reciban la información de conformidad con esta Política, podrán utilizar y procesar tanto los Datos Personales, como la información que no haga identificable al usuario de la Página Web, de forma individual o colectiva para los fines arriba señalados.</p> 
				<span class="gotham-bold font16 sherwood-text block">3. TRANSFERENCIA DE DATOS PERSONALES</span>
				<p class="gotham-book font16 sherwood-text">Salvo por que sea dispuesto de manera distinta en la presente Política o en el aviso de privacidad respectivo, los Datos Personales recabados por Danone se utilizarán para los fines para los que fueron proporcionados, y serán transferidos únicamente a las empresas filiales o subsidiarias de Danone y cada una de las áreas que las integran, así como a las empresas que Danone contrate para procesar y analizar dicha información. El receptor de su información, será solicitado por Nosotros de tratar los Datos Personales con los mismos niveles de seguridad que con los que nosotros los tratamos.</p>
				<span class="gotham-bold font16 sherwood-text block">4. DATOS PERSONALES DE MENORES DE EDAD</span>
				<p class="gotham-book font16 sherwood-text">Para el caso de la obtención de Datos Personales de menores de edad, se deberá de contar con el consentimiento del padre, madre o tutor, y se deberá de proporcionar copia de la documentación que acredite dicha relación (incluyendo sin limitar, acta de nacimiento e identificaciones).</p>
				<span class="gotham-bold font16 sherwood-text block">5. MEDIDAS DE SEGURIDAD</span>
				<p class="gotham-book font16 sherwood-text">Danone protege y continuará protegiendo sus Datos Personales con todos los esfuerzos razonables para asegurarse aquellos recabados en la Página Web o de cualquier otra manera, cumplen con lo dispuesto en la Ley. No obstante lo anterior, sugerimos que la persona que visita la Página Web también cuente con medidas adicionales para proteger su información personal. Danone no se hace responsable por el acceso del visitante a la Página Web a links de terceros de cuyas políticas de privacidad Danone no tenga control.</p>
				<span class="gotham-bold font16 sherwood-text block">6. REVISIÓN Y ACTUALIZACIÓN DE LA INFORMACIÓN RECABADA</span>
				<p class="gotham-book font16 sherwood-text">Los visitantes de la Página Web que provean Datos Personales a Danone, podrán ejercer sus derechos de acceso, rectificación, cancelación y oposición, de conformidad con la Ley en cualquier momento contactando al área legal de Grupo Danone.</p>
				<span class="gotham-bold font16 sherwood-text block">7. ACTUALIZACIONES A ESTA POLÍTICA DE PRIVACIDAD</span>
				<p class="gotham-book font16 sherwood-text">Danone se reserva el derecho de actualizar, ajustar, y modificar estas Políticas en cualquier momento y sin previo aviso. De igual manera, Danone podremos cambiar los productos, políticas, promociones y/o fines de esta Página Web sin previo aviso. Lo invitamos a revisar los cambios a esta Política de manera regular.</p>
				<span class="gotham-bold font16 sherwood-text block">8. CONTACTO</span>
				<p class="gotham-book font16 sherwood-text">Cualquier duda o comentario a las presentes Políticas puede ser atendido por el área Legal o Compliance de Grupo Danone, o enviar un correo a Compliance.MX@danone.com</p>
				<p class="gotham-book font16 sherwood-text">Esta política de privacidad ha sido actualizada en Diciembre 2016.</p>
				<p class="gotham-book font16 sherwood-text">Lea más en http://www.activia.com.mx/content/politicas-de-privacidad#HtFsVbY1R64gTQDe.99</p>
			</div>
			<div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect waves-light btn teak gotham-book text-normal border-white">Cerrar</a>
			</div>
		</div>