
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="logo">
				<img src="<?php echo base_url(); ?>assets/img/logo_activia.svg" />
			</div>
			<div class="row no-padding">
				<div class="col s12 m12 l6 sherwood" id="green-side">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l10 offset-l1">
							<div class="space10"></div>
							<center>
								<div class="space40 hide-on-large-only"></div>
								<span class="gotham-bold font20 white-text block">¡Únete al Reto Activia y gana premios increíbles como un viaje a la Riviera Maya!</span>
								<div class="space10"></div>
								<span class="gotham-light font14 white-text block">Entérate cómo puedes participar.</span>
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/pW4krgJ8IWU?autoplay=0&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
								</div>
								<!--
								<div class="video-container">
									<video class="responsive-video" controls>
										<source src="<?php echo base_url(); ?>assets/video/final_3.mp4" type="video/mp4">
										<source src="<?php echo base_url(); ?>assets/video/final_3.ogv" type="video/ogg">
										<source src="<?php echo base_url(); ?>assets/video/final_3.webm" type="video/webm">
										Tu navegador no soporta video.
									</video>
								</div>-->
								<div class="space20"></div>
								<span class="gotham-light font14 white-text block">Podrás ganar cada semana una membresía de gym o experiencias en SPAs y un premio final ¡Un viaje doble a la Riviera Maya!</span>
								<div class="space10"></div>
								<img src="<?php echo base_url(); ?>assets/img/prices.jpg" class="responsive-img" />
								<div class="space20"></div>
							</center>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6 lilac">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l8 offset-l2">
							<div class="space10"></div>
							<div class="centered">
								<span class="gotham-book font20 sherwood-text">Ingresa y registra tus códigos</span>
							</div>
							<form class="col s12" id="formLogin" name="formLogin" accept-charset="utf-8" method="post">
								<div class="row">
									<div class="input-field col s12">
										<input class="gotham-book" placeholder="Correo Electrónico" id="inputLoginEmail" name="inputLoginEmail" type="text">
    								</div>
									<div class="input-field col s12">
										<input class="gotham-book" placeholder="Contraseña" id="inputLoginPassword" name="inputLoginPassword" type="password">
										<a class="gotham-book font10 sherwood-text" href="<?php echo base_url(); ?>reset">¿Olvidaste tu contraseña?</a>
    								</div>
    								<div class="input-field col s12">
	    								<center>
		    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnLogin" name="btnLogin">Ingresar</a>
	    								</center>
    								</div>
  								</div>
							</form>
							<hr />
							<center>
								<div class="space20"></div>
								<span class="gotham-book font20 sherwood-text">¿Aún no tienes cuenta? <span class="gotham-bold">¡Regístrate!</span></span>
								<div class="space20"></div>
								<a class="waves-effect waves-light btn yale gotham-bold text-normal border-pastel" href="<?php echo $facebook; ?>"><span class="hide-on-large-only">Usa Facebook</span><span class="hide-on-med-and-down">Iniciar sesión con Facebook</span></a>
								<div class="space20"></div>
								<span><hr class="inline-line"></span> <span class="gotham-book-italic sherwood-text">Ó</span> <span><hr class="inline-line"></span>
							</center>
							<form class="col s12" id="formRegister" name="formRegister" accept-charset="utf-8" method="post">
								<div class="row">
									<input type="hidden" id="inputInvite" name="inputInvite" value="" />
									<div class="input-field col s12 m12 l6">
										<input class="gotham-book" autocomplete="off" placeholder="Nombre*" id="inputName" name="inputName" type="text">
    								</div>
    								<div class="input-field col s12 m12 l6">
										<input class="gotham-book" autocomplete="off" placeholder="Apellidos*" id="inputLastName" name="inputLastName" type="text">
    								</div>
									<div class="input-field col s12">
										<input class="gotham-book" autocomplete="off" placeholder="Correo Electrónico*" id="inputEmail" name="inputEmail" type="text">
    								</div>
									<div class="input-field col s12">
										<input class="gotham-book" autocomplete="off" placeholder="Contraseña*" id="inputPassword" name="inputPassword" type="password">
    								</div>
    								<div class="input-field col s12">
	    								<input type="checkbox" id="inputTerms" value="inputTerms" value="1" />
										<label for="inputTerms">Acepto los Términos y Condiciones.</label>
										<input type="checkbox" id="inputPrivacy" value="inputPrivacy" value="1" />
										<label for="inputPrivacy">Acepto las Políticas de Privacidad.</label>
    								</div>
    								<div class="input-field col s12">
	    								<center>
		    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnRegister" name="btnRegister">Enviar</a>
	    								</center>
    								</div>
  								</div>
							</form>
							<div class="space60"></div>
							<center>
								<span class="gotham-book font16 sherwood-text block underline"><a class="sherwood-text" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a> · <a class="sherwood-text" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></span>
							</center>
						</div>
					</div>
				</div>
			</div>		
		</div>