
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<!--<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>logout">Cerrar Sesión</a>-->
					<div class="space40"></div>
					<div class="row">
						<div class="col s12 m12 l4 offset-l4">
							<div class="space30"></div>
							<div class="centered">
								<span class="gotham-book font20 white-text">Inicia Sesión para continuar</span>
							</div>
							<form class="col s12" id="formAdmin" name="formAdmin" accept-charset="utf-8" method="post">
								<div class="row">
									<div class="input-field col s12">
										<input class="gotham-book" placeholder="Nombre de Usuario" id="inputAdminUsername" name="inputAdminUsername" type="text">
    								</div>
									<div class="input-field col s12">
										<input class="gotham-book" placeholder="Contraseña" id="inputAdminPassword" name="inputAdminPassword" type="password">
										<a class="gotham-book font10 sherwood-text" href="<?php echo base_url(); ?>reset">¿Olvidaste tu contraseña?</a>
    								</div>
    								<div class="input-field col s12">
	    								<center>
		    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnAdmin" name="btnAdmin">Ingresar</a>
	    								</center>
    								</div>
  								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>