
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<div class="container">
						<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>close">Cerrar Sesión</a>
						<a class="gotham-book font16 white-text block underline codes" href="<?php echo base_url(); ?>fails">Códigos Erróneos</a>
						<a class="gotham-book font16 white-text block underline records" href="<?php echo base_url(); ?>users/records">Usuarios Registrados</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 480px; margin-top: 20px;" href="<?php echo base_url(); ?>users/codes">Códigos Válidos</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 620px; margin-top: 20px;" href="<?php echo base_url(); ?>users/assign">Asignar Código</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 760px; margin-top: 20px;" href="<?php echo base_url(); ?>users/valid">Códigos Registrados</a>
						<div class="space20"></div>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="space60"></div>
								<div class="right" style="margin-right: 10px;">
									<form id="formSearch" name="formSearch" accept-charset="utf-8" method="post">
										<div class="input-field col s12">
											<input class="gotham-book" placeholder="Email a buscar" autocomplete="off" id="inputEmail" name="inputEmail" type="text">
		    							</div>
									</form>
								</div>
								<span class="gotham-book font20 white-text">Lista de Registros de Códigos por Semana</span>
							</div>
						</div>
						<div class="row" id="results">
							<div class="col s12 m12 l12 white">
								<ul class="tabs">
									<li class="tab col s2"><a class="teak-text" href="#semana0">Semana 0</a></li>
									<li class="tab col s2"><a class="teak-text" href="#semana1">Semana 1</a></li>
									<li class="tab col s2"><a class="teak-text" href="#semana2">Semana 2</a></li>
									<li class="tab col s2"><a class="teak-text" href="#semana3">Semana 3</a></li>
									<li class="tab col s2"><a class="teak-text" href="#semana4">Semana 4</a></li>
									<li class="tab col s2"><a class="teak-text" href="#ranking">Ranking</a></li>
      							</ul>
    						</div>
							<div id="semana0" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Semana 0, antes del 16 de Enero</p>
									<div class="space20"></div>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($cero as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1 AND log.createdAt BETWEEN '2016-12-01 00:00:00' AND '2017-01-15 23:59:59'");
										    ?>
											<div class="collapsible-body teak white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<table class="centered responsive-table" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
							<div id="semana1" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Semana 1, del 16 de Enero al 22 de Enero</p>
									<div class="space20"></div>
									<?php if (count($uno) > 0) { ?>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($uno as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1 AND log.createdAt BETWEEN '2017-01-16 00:00:00' AND '2017-01-22 23:59:59'");
										    ?>
											<div class="collapsible-body white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<div class="right">
													<div class="space10"></div>
													<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php echo base_url() . 'users/week/' . $user->iduser . '/1'; ?>">Descargar XLS con Puntos de la Semana</a>
													<div class="space10"></div>
												</div>
												<table class="centered responsive-table teak" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<?php } else { ?>
									<p class="gotham-book teak-text">Aún no hay participantes en esta semana.</p>
									<?php } ?>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
							<div id="semana2" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Semana 2, del 23 de Enero al 29 de Enero</p>
									<div class="space20"></div>
									<?php if (count($dos) > 0) { ?>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($dos as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1 AND log.createdAt BETWEEN '2017-01-23 00:00:00' AND '2017-01-29 23:59:59'");
										    ?>
											<div class="collapsible-body white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<div class="right">
													<div class="space10"></div>
													<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php echo base_url() . 'users/week/' . $user->iduser . '/2'; ?>">Descargar XLS con Puntos de la Semana</a>
													<div class="space10"></div>
												</div>
												<table class="centered responsive-table teak" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<?php } else { ?>
									<p class="gotham-book teak-text">Aún no hay participantes en esta semana.</p>
									<?php } ?>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
							<div id="semana3" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Semana 3, del 30 de Enero al 05 de Febrero</p>
									<div class="space20"></div>
									<?php if (count($tres) > 0) { ?>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($tres as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1 AND log.createdAt BETWEEN '2017-01-30 00:00:00' AND '2017-02-05 23:59:59'");
										    ?>
											<div class="collapsible-body white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<div class="right">
													<div class="space10"></div>
													<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php echo base_url() . 'users/week/' . $user->iduser . '/3'; ?>">Descargar XLS con Puntos de la Semana</a>
													<div class="space10"></div>
												</div>
												<table class="centered responsive-table teak" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<?php } else { ?>
									<p class="gotham-book teak-text">Aún no hay participantes en esta semana.</p>
									<?php } ?>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
							<div id="semana4" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Semana 4, del 06 de Febrero al 12 de Febrero</p>
									<div class="space20"></div>
									<?php if (count($cuatro) > 0) { ?>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($cuatro as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1 AND log.createdAt BETWEEN '2017-02-06 00:00:00' AND '2017-02-12 23:59:59'");
										    ?>
											<div class="collapsible-body white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<div class="right">
													<div class="space10"></div>
													<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php echo base_url() . 'users/week/' . $user->iduser . '/4'; ?>">Descargar XLS con Puntos de la Semana</a>
													<div class="space10"></div>
												</div>
												<table class="centered responsive-table teak" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<?php } else { ?>
									<p class="gotham-book teak-text">Aún no hay participantes en esta semana.</p>
									<?php } ?>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
							<div id="ranking" class="col s12 m12 l12 white">
								<div class="white">
									<p class="gotham-book font20 teak-text">Top 50, sin delimitación de fechas y ordenados por puntos</p>
									<div class="space20"></div>
									<?php if (count($ranking) > 0) { ?>
									<ul class="collapsible" data-collapsible="accordion">
										<?php foreach ($ranking as $user) { ?>
										<li>
									    	<div class="collapsible-header"><i class="material-icons">account_circle</i><?php echo $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' ) - ' . $user->points . ' pts'; ?></div>
									    	<?php 
										    	//Consultamos sus puntos 
										    	$query = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1");
										    ?>
											<div class="collapsible-body white-text">
												<?php if ($query->num_rows() > 0) { ?>
												<div class="right">
													<div class="space10"></div>
													<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" href="<?php echo base_url() . 'users/ranking/' . $user->iduser; ?>">Descargar XLS con Puntos de todas las semanas</a>
													<div class="space10"></div>
												</div>
												<table class="centered responsive-table teak" id="table_historial">
													<thead>
														<tr>
															<th class="gotham-bold white-text">Acción</th>
															<th class="gotham-bold white-text">Valor</th>
															<th class="gotham-bold white-text">Puntos</th>
															<th class="gotham-bold white-text">Producto</th>
															<th class="gotham-bold white-text">Fecha</th>
															<th class="gotham-bold white-text">IP</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($query->result() as $log) { ?>
														<tr>
															<td class="gotham-book white-text">
																<?php 
																	switch ($log->action) {
																		case 'invite': echo 'Share Invitación'; break;
																		case 'code': echo 'Código Registrado'; break;
																		case 'assign': echo 'Código Asignado'; break;
																	}
																?>
															</td>
															<td class="gotham-book white-text"><?php echo $log->value; ?></td>
															<td class="gotham-book white-text"><?php echo $log->points; ?> pts</td>
															<td class="gotham-book white-text"><?php echo $log->product; ?></td>
															<td class="gotham-book white-text"><?php echo $log->createdAt; ?></td>
															<td class="gotham-book white-text"><?php echo $log->ip; ?></td>
							    						</tr>
							    						<?php } ?>
													</tbody>
												</table>
												<?php } else { ?>
												<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>
												<?php } ?>
											</div>
									    </li>
									    <?php } ?>
									</ul>
									<?php } else { ?>
									<p class="gotham-book teak-text">Aún no hay participantes en esta semana.</p>
									<?php } ?>
									<div class="space20"></div>
									<!--<div class="row" id="pagination">
										<div class="left" style="margin-left: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Anterior</a>
										</div>
										<?php if (count($cero_all) > 10) { ?>
										<div class="right" style="margin-right: 10px; ">
											<div class="space20"></div>
											<a href="#" class="waves-effect waves-light btn teak gotham-book text-normal border-white">Siguiente</a>
										</div>
										<?php } ?>
									</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>