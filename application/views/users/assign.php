
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<div class="container">
						<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>close">Cerrar Sesión</a>
						<a class="gotham-book font16 white-text block underline codes" href="<?php echo base_url(); ?>fails">Códigos Erróneos</a>
						<a class="gotham-book font16 white-text block underline records" href="<?php echo base_url(); ?>users/records">Usuarios Registrados</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 480px; margin-top: 20px;" href="<?php echo base_url(); ?>users/codes">Códigos Válidos</a>
						<div class="space20"></div>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="space60"></div>
								<span class="gotham-book font20 white-text">Asignación de Código por Usuario</span>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l8 offset-l2">
								<form class="col s12" id="formAssign" name="formAssign" accept-charset="utf-8" method="post">
									<div class="row">
										<div class="input-field col s12">
											<select class="gotham-book" id="inputUser" name="inputUser">
												<option value="">Selecciona al participante</option>
												<?php foreach ($users as $user) { ?>
												<option value="<?php echo $user->iduser; ?>"><?php echo $user->name.' '.$user->lastname.' ('.$user->email.')'; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="input-field col s12">
											<select class="gotham-book" id="inputProducto" name="inputProducto">
												<option value="">Selecciona tu producto a registrar</option>
												<option value="bebible">Bebible (clásicos, cereales)</option>
												<option value="bebible">Bebible (0%)</option>
												<option value="semillas">Semillas (chía y quinoa)</option>
												<option value="cereales">Con cereales a un lado</option>
												<option value="kilo">Kilo</option>
												<option value="vasito">Vasito</option>
											</select>
										</div>
										<div class="input-field col s4">
											<input class="gotham-book" autocomplete="off" maxlength="7" placeholder="Fecha" id="inputDate" name="inputDate" type="text">
										</div>
										<div class="input-field col s4">
											<input class="gotham-book" autocomplete="off" maxlength="4" placeholder="Folio" id="inputFolio" name="inputFolio" type="text">
										</div>
										<div class="input-field col s4">
											<input class="gotham-book" autocomplete="off" maxlength="5" placeholder="HH:SS" id="inputHora" name="inputHora" type="text">
										</div>
										<div class="input-field col s12">
											<center>
												<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnAssign">Asignar Código</a>
											</center>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>