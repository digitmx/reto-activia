<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="row no-padding">
				<div class="col s12 m12 l12 sherwood" id="green-side">
					<div class="container">
						<a class="gotham-book font16 white-text block underline logout" href="<?php echo base_url(); ?>close">Cerrar Sesión</a>
						<a class="gotham-book font16 white-text block underline codes" href="<?php echo base_url(); ?>fails">Códigos Erróneos</a>
						<a class="gotham-book font16 white-text block underline records" href="<?php echo base_url(); ?>users/records">Usuarios Registrados</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 480px; margin-top: 20px;" href="<?php echo base_url(); ?>users/codes">Códigos Válidos</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 620px; margin-top: 20px;" href="<?php echo base_url(); ?>users/assign">Asignar Código</a>
						<a class="gotham-book font16 white-text block underline" style="position: absolute; right: 760px; margin-top: 20px;" href="<?php echo base_url(); ?>users/valid">Códigos Registrados</a>
						<div class="space20"></div>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="space60"></div>
								<span class="gotham-book font20 white-text">Lista de Códigos Registrados en el Sistema</span>
							</div>
						</div>
						<div class="row" id="results">
							<div class="col s12 m12 l12 white">
								<ul class="tabs">
									<li class="tab col s1"><a href="#L10">10</a></li>
									<li class="tab col s1"><a href="#L14">14</a></li>
									<li class="tab col s2"><a href="#L15">15</a></li>
									<li class="tab col s2"><a href="#L17">17</a></li>
									<li class="tab col s2"><a href="#L21">21</a></li>
									<li class="tab col s2"><a href="#L22">22</a></li>
									<li class="tab col s2"><a href="#L23">23</a></li>
					      		</ul>
					    	</div>
							<div id="L10" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL10" name="formNewCodeL10" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L10" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="500" />
										<input type="hidden" id="product" name="product" value="BEBIBLES 170g." />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL10" name="btnNewCodeL10">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L10</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l10 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L14" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL14" name="formNewCodeL14" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L14" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="500" />
										<input type="hidden" id="product" name="product" value="BEBIBLES 240g." />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL14" name="btnNewCodeL14">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L14</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l14 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L15" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL15" name="formNewCodeL15" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L15" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="500" />
										<input type="hidden" id="product" name="product" value="BEBIBLES 240g." />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL15" name="btnNewCodeL15">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L15</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l15 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L17" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL17" name="formNewCodeL17" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L17" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="500" />
										<input type="hidden" id="product" name="product" value="BEBIBLES 240g." />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL17" name="btnNewCodeL17">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L17</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l17 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L21" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL21" name="formNewCodeL21" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L21" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="250" />
										<input type="hidden" id="product" name="product" value="SOLIDO 120g." />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL21" name="btnNewCodeL21">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L21</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l21 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L22" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL22" name="formNewCodeL22" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L22" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="400" />
										<input type="hidden" id="product" name="product" value="COS (Cereal on the side)" />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL22" name="btnNewCodeL22">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L22</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l22 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
							<div id="L23" class="col s12 white">
								<div class="white">
									<form id="formNewCodeL23" name="formNewCodeL23" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>users/new_code">
										<input type="hidden" id="folio" name="folio" value="L23" />
										<input type="hidden" id="start" name="start" value="00:00" />
										<input type="hidden" id="end" name="end" value="24:00" />
										<input type="hidden" id="points" name="points" value="1000" />
										<input type="hidden" id="product" name="product" value="Kilos" />
										<div class="row">
											<div class="input-field col s12 m12 l6">
												<input class="gotham-book" placeholder="Fecha" id="inputDate" name="inputDate" type="text" autocomplete="off">
		    								</div>
		    								<div class="input-field col s12 m12 l6">
												<center>
				    								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" id="btnNewCodeL23" name="btnNewCodeL23">Agregar nuevo código</a>
			    								</center>
		    								</div>
										</div>
									</form>
									<p class="gotham-book font20 teak-text">Códigos de la Línea de Producción L23</p>
									<div class="space20"></div>
									<table>
								        <thead>
								        	<tr>
								            	<th data-field="date">Fecha</th>
												<th data-field="folio">Folio</th>
												<th data-field="start">Inicio</th>
												<th data-field="end">Termino</th>
												<th data-field="points">Puntos</th>
								        	</tr>
								        </thead>
								
								        <tbody>
									        <?php foreach ($code_l23 as $row) { ?>
								        	<tr>
								            	<td><?php echo $row->date; ?></td>
												<td><?php echo $row->folio; ?></td>
												<td><?php echo $row->start; ?></td>
												<td><?php echo $row->end; ?></td>
												<td><?php echo $row->points; ?></td>
								        	</tr>
								        	<?php } ?>
								        </tbody>
								    </table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>