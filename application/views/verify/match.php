
		<div class="container-fluid white">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="space20"></div>
					<center>
						<span class="gotham-book font30 jumbo-text">Únete al reto Activia</span>
						<hr class="line" />
					</center>
					<div class="space20"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid lilac no-margin-row">
			<div class="logo">
				<img src="<?php echo base_url(); ?>/assets/img/logo_activia.svg" />
			</div>
			<div class="row no-padding">
				<div class="col s12 m12 l6 sherwood" id="green-side">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l10 offset-l1">
							<div class="space10"></div>
							<center>
								<div class="space40 hide-on-large-only"></div>
								<span class="gotham-bold font20 white-text block">¡Únete al Reto Activia y gana premios increíbles como un viaje a la Riviera Maya!</span>
								<div class="space10"></div>
								<span class="gotham-light font20 white-text block">Entérate cómo puedes participar.</span>
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/pW4krgJ8IWU?autoplay=0&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
								</div>
								<!--
								<div class="video-container">
									<video class="responsive-video" controls>
										<source src="<?php echo base_url(); ?>assets/video/final_3.mp4" type="video/mp4">
										<source src="<?php echo base_url(); ?>assets/video/final_3.ogv" type="video/ogg">
										<source src="<?php echo base_url(); ?>assets/video/final_3.webm" type="video/webm">
										Tu navegador no soporta video.
									</video>
								</div>-->
								<div class="space20"></div>
								<span class="gotham-light font14 white-text block">Podrás ganar cada semana una membresía de gym o experiencias en SPAs y un premio final ¡Un viaje doble a la Riviera Maya!</span>
								<div class="space10"></div>
								<img src="<?php echo base_url(); ?>assets/img/prices.jpg" class="responsive-img" />
								<div class="space20"></div>
							</center>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6 lilac">
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 l8 offset-l2">
							<div class="space60"></div>
							<center>
								<span class="gotham-book font20 sherwood-text block">El código leído no corresponde al de la sesión activa.</span>
								<div class="space20"></div>
								<span class="gotham-book font20 sherwood-text block">Reenvia tu correo de verificación para obtener tu código correcto.</span>
								<div class="space40"></div>
								<a class="waves-effect waves-light btn teak gotham-book text-normal border-white" rel="<?php echo $_SESSION['user']['iduser']; ?>" id="btnResendVerify" name="btnResentVerify" href="#">Reenviar correo de verificación</a>
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>