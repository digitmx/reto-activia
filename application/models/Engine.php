<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

class Engine extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$output = FALSE;
		
		// createUser
		if ($msg == 'createUser')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$lastname = (isset($fields['lastname'])) ? (string)trim($fields['lastname']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			$invite = (isset($fields['invite'])) ? (string)trim($fields['invite']) : '';
			$ip = '0.0.0.0';
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $ip = $_SERVER['REMOTE_ADDR'];
			}
			
			//Verificamos
			if ($name && $lastname && $email && $password)
			{
				//Verificamos si el usuario no existe
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() == 0)
				{
					//Registramos al Usuario
					$data = array(
						'name' => $name,
						'lastname' => $lastname,
						'email' => $email,
						'password' => sha1($password),
						'createdAt' => date('Y-m-d H:i:s'),
						'invite' => $invite,
						'verified' => 0,
						'ip' => $ip,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Checamos si el registro fue con invitacion
					if ($invite)
					{
						//Consultamos los Puntos por invitaciones
						$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $invite . "' AND user.status = 1");
						
						//Verificamos que sea de las primeras 5 invitaciones
						if ($query_invitaciones->num_rows() < 5)
						{
							//Consultamos al dueño de la invitación
							$query_invite = $this->db->query("SELECT * FROM user WHERE user.code = '" . $invite . "' AND user.status = 1");
							$row_invite = $query_invite->row();
							
							//Registramos la invitación en el Log
							$data = array(
								'iduser' => $row_invite->iduser,
								'points' => '100',
								'action' => 'invite',
								'value' => $email,
								'createdAt' => date('Y-m-d H:i:s'),
								'status' => 1
							);
							$this->db->insert('log', $data);
						}
					}
					
					//Código
					$code = sha1($iduser);
					$code = substr($code, 0, 8);
					
					//Creamos el código de invite
					$data = array(
						'code' => $code
					);
					$this->db->where('iduser', $iduser);
					$this->db->update('user', $data);
					
					//ENVIAMOS CORREO DE BIENVENIDA
					$name_array = explode(' ', $name);
					$data['name'] = $name_array[0];
					$data['code'] = $code;
					
					//Correo de Notificación
					$subject = 'Bienvenida al #RetoActivia';
					$body = $this->load->view('mail/bienvenida', $data, TRUE);
					$altbody = strip_tags($body);
			
					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
				        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
				        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email);
				        //$mail->AddBCC('milio.hernandez@gmail.com');
			
				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}
					
					//Creamos la SESSION
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
					    'name' => $name,
					    'lastname' => $lastname,
					    'code' => $code
					);

					//Save Admin in $_SESSION
					$this->session->set_userdata('user', $data);
					$this->session->set_userdata('ra_logged', true);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)2,
						'msg' => (string)'Este participante ya está registrado, inicia sesión.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginUser
		if ($msg == 'loginUser')
		{
			//Leemos los Datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos
			if ($email && $password)
			{
				//Verificamos si el usuario existe
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' AND status = 1 LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos la Contraseña
					if ($row->password == sha1($password))
					{
						//Creamos la SESSION
						$data = array(
							'iduser' => $row->iduser,
							'email' => $row->email,
						    'name' => $row->name,
						    'lastname' => $row->lastname,
						    'code' => $row->code,
						    'verified' => $row->verified
						);
	
						//Save Admin in $_SESSION
						$this->session->set_userdata('user', $data);
						$this->session->set_userdata('ra_logged', true);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)3,
							'msg' => (string)'Contraseña incorrecta, intenta de nuevo.'
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)2,
						'msg' => (string)'Este participante no existe, regístrate.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// resetPassword
		if ($msg == 'resetPassword')
		{
			//Leemos los Datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			
			//Verificamos
			if ($email)
			{
				//Verificamos si el usuario existe
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' AND status = 1 LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos que el Usuario no sea de Facebook
					if ((string)trim($row->fbid))
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Este usuario usa Facebook para iniciar sesión.'
						);
					}
					else
					{
						//Token
						$token = sha1(now());
						//$token = substr($code, 0, 8);
						
						//Creamos el código de invite
						$data = array(
							'token' => $token
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Enviamos el Correo
						$name = $row->name;
						$name_array = explode(' ', $name);
						$data['name'] = $name_array[0];
						$data['token'] = $token;
						
						//Correo de Notificación
						$subject = 'Restablecer Contraseña';
						$body = $this->load->view('mail/reset', $data, TRUE);
						$altbody = strip_tags($body);
						$email = $row->email;
				
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
					        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
					        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email);
					        //$mail->AddBCC('milio.hernandez@gmail.com');
				
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se enviará un correo electrónico con un link para restablecer la contraseña.'
						);
					}

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)2,
						'msg' => (string)'Este participante no existe, regístrate.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// newPassword
		if ($msg == 'newPassword')
		{
			//Leemos los Datos
			$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos
			if ($token && $password)
			{
				//Verificamos si el usuario existe
				$query = $this->db->query("SELECT * FROM user WHERE token = '" . $token . "' AND status = 1 LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Creamos el código de invite
					$data = array(
						'password' => sha1($password),
						'token' => ''
					);
					$this->db->where('iduser', $row->iduser);
					$this->db->update('user', $data);
					
					//Mostrar Error
					$array = array(
						'status' => (int)1,
						'msg' => (string)'Se ha actualizado la contraseña. Inicia sesión de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)2,
						'msg' => (string)'Este token no existe o ya fue utilizado.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// addCode
		if ($msg == 'addCode')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$producto = (isset($fields['producto'])) ? (string)trim($fields['producto']) : '';
			$nombre_producto = (isset($fields['nombre_producto'])) ? (string)trim($fields['nombre_producto']) : '';
			$fecha = (isset($fields['fecha'])) ? (string)trim($fields['fecha']) : '';
			$fecha = strtoupper($fecha);
			$folio = (isset($fields['folio'])) ? (string)trim($fields['folio']) : '';
			$folio = strtoupper($folio);
			$count_folio = strlen($folio); $count_folio = $count_folio-3;
			$folio_final = mb_substr($folio,$count_folio,3);
			$hora = (isset($fields['hora'])) ? (string)trim($fields['hora']) : '';
			$hora = strtoupper($hora);
			$ip = '0.0.0.0';
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $ip = $_SERVER['REMOTE_ADDR'];
			}
			
			//Verificamos la fecha
			if (date('Y-m-d H:i:s') < '2017-02-13 00:00:00')
			{
				//Verificamos los Datos
				if ($iduser && $producto && $fecha && $folio && $hora)
				{
					//Consultamos al usuario
					$query_user = $this->db->query("SELECT * FROM user WHERE user.iduser = " . $iduser . " AND user.status = 1 LIMIT 1");
					
					//Verificamos
					if ($query_user->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row_user = $query_user->row();
						
						//Calculamos los puntos
						$puntos = 0;
						switch ($producto)
						{
							case 'bebible': $puntos = 500; break;
							case 'semillas': $puntos = 500; break;
							case 'cereales': $puntos = 400; break;
							case 'kilo': $puntos = 1000; break;
							case 'vasito': $puntos = 250; break;
							default: $puntos = 0; break;
						}
						
						//Verificamos los puntos asignados
						if ($puntos)
						{
							//Consultamos el código
							$query_code = $this->db->query("SELECT * FROM code WHERE code.date = '" . $fecha . "' AND code.folio = '". $folio_final . "' AND code.start <= '" . $hora . "' AND code.end > '" . $hora . "' AND code.status = 1 LIMIT 1");
							
							//Verificamos que exista el codigo
							if ($query_code->num_rows() > 0)
							{
								//Leemos el código
								$row_code = $query_code->row();
								
								//Verificamos puntos del producto seleccionado con los puntos en base de datos
								if ($puntos == $row_code->points)
								{
									//Consultamos si el código no ha sido utilizado más de 3 veces por el mismo usuario
									$query_log = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.value = '" . $fecha . " " . $folio . " " . $hora . "' AND log.status = 1");
									
									//Verificamos el número de códigos repetidos por usuario
									if ($query_log->num_rows() < 3)
									{
										//Creamos el registro de puntos
										$data = array(
											'iduser' => $iduser,
											'points' => $row_code->points,
											'action' => 'code',
											'value' => $fecha . ' ' . $folio . ' ' . $hora,
											'product' => $nombre_producto,
											'ip' => $ip,
											'createdAt' => date('Y-m-d H:i:s'),
											'status' => 1
										);
										$this->db->insert('log', $data);
										$idcode = $this->db->insert_id();
										
										//ENVIAMOS CORREO DE PUNTOS
										$subject = 'Gracias por tu registro #RetoActivia';
										$body = $this->load->view('mail/code', $data, TRUE);
										$altbody = strip_tags($body);
										$email = $row_user->email;
								
										//Verificamos
										if ($subject != '' && $body != '' && $altbody != '' && $email != '')
										{
											//Mandamos el Correo
											$mail = new PHPMailer(true);
											$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
											$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
											$mail->CharSet = 'UTF-8';
											$mail->Encoding = "quoted-printable";
									        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
									        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
									        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
									        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
									        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
									        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
									        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
									        $mail->SMTPDebug  = 0;
									        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
									        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
									        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
									        $mail->Body      = $body;
									        $mail->AltBody    = $altbody;
									        $mail->AddAddress($email);
									        //$mail->AddBCC('milio.hernandez@gmail.com');
								
									        if ($mail->Send())
											{
												$response = true;
											}
											else
											{
												$response = false;
											}
										}
										
										//Consultamos sus puntos en base de datos
										$puntos_invitaciones = 0;
										$puntos_codigos = 0;
										
										//Consultamos los Puntos por invitaciones
										$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
										
										//Calculamos los Puntos por invitaciones
										if ($query_invitaciones->num_rows() > 5)
										{
											//Asignamos el monto máximo de invitaciones
											$puntos_invitaciones = 500;
										}
										else
										{
											//Calculamos los puntos por invitación
											$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
										}
										
										//Consultamos los Puntos por Códigos
										$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
										
										//Procesamos los Puntos
										foreach ($query_puntos->result() as $row_puntos)
										{
											//Acumulamos
											$puntos_codigos = $puntos_codigos + $row_puntos->points;
										}
										
										//Asignamos los puntos
										$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
										
										//Generamos el Arreglo
										$array = array(
											'status' => (int)1,
											'msg' => 'El código ha sido registrado exitosamente.',
											'data' => $data
										);
					
										//Imprimimos el Arreglo
										$this->printJSON($array);
										$output = TRUE;
									}
									else
									{
										//Consultamos sus puntos en base de datos
										$puntos_invitaciones = 0;
										$puntos_codigos = 0;
										
										//Consultamos los Puntos por invitaciones
										$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
										
										//Calculamos los Puntos por invitaciones
										if ($query_invitaciones->num_rows() > 5)
										{
											//Asignamos el monto máximo de invitaciones
											$puntos_invitaciones = 500;
										}
										else
										{
											//Calculamos los puntos por invitación
											$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
										}
										
										//Consultamos los Puntos por Códigos
										$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
										
										//Procesamos los Puntos
										foreach ($query_puntos->result() as $row_puntos)
										{
											//Acumulamos
											$puntos_codigos = $puntos_codigos + $row_puntos->points;
										}
										
										//Asignamos los puntos
										$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
										
										//Creamos el registro de error
										$data = array(
											'iduser' => $iduser,
											'points' => $puntos,
											'action' => 'repeated',
											'value' => $fecha . ' ' . $folio . ' ' . $hora,
											'product' => $nombre_producto,
											'ip' => $ip,
											'createdAt' => date('Y-m-d H:i:s'),
											'status' => 1
										);
										$this->db->insert('fail', $data);
								
										//Mostrar Error
										$array = array(
											'status' => (int)4,
											'msg' => (string)'Código repetido.',
											'data' => $data
										);
						
										//Imprimimos el Arreglo
										$this->printJSON($array);
										$output = TRUE;
									}
								}
								else
								{
									//Consultamos sus puntos en base de datos
									$puntos_invitaciones = 0;
									$puntos_codigos = 0;
									
									//Consultamos los Puntos por invitaciones
									$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
									
									//Calculamos los Puntos por invitaciones
									if ($query_invitaciones->num_rows() > 5)
									{
										//Asignamos el monto máximo de invitaciones
										$puntos_invitaciones = 500;
									}
									else
									{
										//Calculamos los puntos por invitación
										$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
									}
									
									//Consultamos los Puntos por Códigos
									$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
									
									//Procesamos los Puntos
									foreach ($query_puntos->result() as $row_puntos)
									{
										//Acumulamos
										$puntos_codigos = $puntos_codigos + $row_puntos->points;
									}
									
									//Asignamos los puntos
									$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
									
									//Creamos el registro de error
									$data = array(
										'iduser' => $iduser,
										'points' => $puntos,
										'action' => 'code',
										'value' => $fecha . ' ' . $folio . ' ' . $hora,
										'product' => $nombre_producto,
										'ip' => $ip,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('fail', $data);
									
									//Mostrar Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'El producto seleccionado no corresponde al folio ingresado, intenta con otro.',
										'data' => $data
									);
					
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Consultamos sus puntos en base de datos
								$puntos_invitaciones = 0;
								$puntos_codigos = 0;
								
								//Consultamos los Puntos por invitaciones
								$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
								
								//Calculamos los Puntos por invitaciones
								if ($query_invitaciones->num_rows() > 5)
								{
									//Asignamos el monto máximo de invitaciones
									$puntos_invitaciones = 500;
								}
								else
								{
									//Calculamos los puntos por invitación
									$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
								}
								
								//Consultamos los Puntos por Códigos
								$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
								
								//Procesamos los Puntos
								foreach ($query_puntos->result() as $row_puntos)
								{
									//Acumulamos
									$puntos_codigos = $puntos_codigos + $row_puntos->points;
								}
								
								//Asignamos los puntos
								$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
								
								//Creamos el registro de error
								$data = array(
									'iduser' => $iduser,
									'points' => $puntos,
									'action' => 'code',
									'value' => $fecha . ' ' . $folio . ' ' . $hora,
									'product' => $nombre_producto,
									'ip' => $ip,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('fail', $data);
										
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Este código no es válido, intenta con otro.',
									'data' => $data
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Consultamos sus puntos en base de datos
							$puntos_invitaciones = 0;
							$puntos_codigos = 0;
							
							//Consultamos los Puntos por invitaciones
							$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
							
							//Calculamos los Puntos por invitaciones
							if ($query_invitaciones->num_rows() > 5)
							{
								//Asignamos el monto máximo de invitaciones
								$puntos_invitaciones = 500;
							}
							else
							{
								//Calculamos los puntos por invitación
								$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
							}
							
							//Consultamos los Puntos por Códigos
							$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
							
							//Procesamos los Puntos
							foreach ($query_puntos->result() as $row_puntos)
							{
								//Acumulamos
								$puntos_codigos = $puntos_codigos + $row_puntos->points;
							}
							
							//Asignamos los puntos
							$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
							
							//Creamos el registro de error
							$data = array(
								'iduser' => $iduser,
								'points' => $puntos,
								'action' => 'code',
								'value' => $fecha . ' ' . $folio . ' ' . $hora,
								'ip' => $ip,
								'createdAt' => date('Y-m-d H:i:s'),
								'status' => 1
							);
							$this->db->insert('error', $data);
										
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Producto no válido.',
								'data' => $data
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Consultamos sus puntos en base de datos
						$puntos_invitaciones = 0;
						$puntos_codigos = 0;
						
						//Consultamos los Puntos por invitaciones
						$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
						
						//Calculamos los Puntos por invitaciones
						if ($query_invitaciones->num_rows() > 5)
						{
							//Asignamos el monto máximo de invitaciones
							$puntos_invitaciones = 500;
						}
						else
						{
							//Calculamos los puntos por invitación
							$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
						}
						
						//Consultamos los Puntos por Códigos
						$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
						
						//Procesamos los Puntos
						foreach ($query_puntos->result() as $row_puntos)
						{
							//Acumulamos
							$puntos_codigos = $puntos_codigos + $row_puntos->points;
						}
						
						//Asignamos los puntos
						$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
						
						//Creamos el registro de error
						$data = array(
							'iduser' => '0',
							'points' => '0',
							'action' => 'code',
							'value' => $fecha . ' ' . $folio . ' ' . $hora,
							'ip' => $ip,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('error', $data);
										
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario esta deshabilitado.',
							'data' => $data
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Consultamos sus puntos en base de datos
					$puntos_invitaciones = 0;
					$puntos_codigos = 0;
					
					//Consultamos los Puntos por invitaciones
					$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
					
					//Calculamos los Puntos por invitaciones
					if ($query_invitaciones->num_rows() > 5)
					{
						//Asignamos el monto máximo de invitaciones
						$puntos_invitaciones = 500;
					}
					else
					{
						//Calculamos los puntos por invitación
						$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
					}
					
					//Consultamos los Puntos por Códigos
					$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
					
					//Procesamos los Puntos
					foreach ($query_puntos->result() as $row_puntos)
					{
						//Acumulamos
						$puntos_codigos = $puntos_codigos + $row_puntos->points;
					}
					
					//Asignamos los puntos
					$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
					
					//Creamos el registro de error
					$data = array(
						'iduser' => $iduser,
						'points' => '0',
						'action' => 'code',
						'value' => $fecha . ' ' . $folio . ' ' . $hora,
						'ip' => $ip,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('error', $data);
										
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Tienes que llenar todos los campos.',
						'data' => $data
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Consultamos sus puntos en base de datos
				$puntos_invitaciones = 0;
				$puntos_codigos = 0;
				
				//Consultamos los Puntos por invitaciones
				$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
				
				//Calculamos los Puntos por invitaciones
				if ($query_invitaciones->num_rows() > 5)
				{
					//Asignamos el monto máximo de invitaciones
					$puntos_invitaciones = 500;
				}
				else
				{
					//Calculamos los puntos por invitación
					$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
				}
				
				//Consultamos los Puntos por Códigos
				$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
				
				//Procesamos los Puntos
				foreach ($query_puntos->result() as $row_puntos)
				{
					//Acumulamos
					$puntos_codigos = $puntos_codigos + $row_puntos->points;
				}
				
				//Asignamos los puntos
				$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
									
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'La promoción ha llegado a su fin, consulta a los ganadores en nuestras redes sociales.',
					'data' => $data
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginAdmin
		if ($msg == 'loginAdmin')
		{
			//Leemos los Datos
			$username = (isset($fields['username'])) ? (string)trim($fields['username']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos
			if ($username && $password)
			{
				//Verificamos si el usuario existe
				$query = $this->db->query("SELECT * FROM admin WHERE username = '" . $username . "' AND password = '" . sha1($password) ."' AND status = 1 LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Creamos la SESSION
					$data = array(
						'idadmin' => $row->idadmin,
						'username' => $row->username
					);

					//Save Admin in $_SESSION
					$this->session->set_userdata('admin', $data);
					$this->session->set_userdata('raa_logged', true);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)2,
						'msg' => (string)'Este administrador no existe.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//queryUser
		if ($msg == 'queryUser')
		{
			//Leemos los datos
			$query = (isset($fields['query'])) ? (string)trim($fields['query']) : '';
			
			//Verificamos
			if ($query)
			{
				//Consultamos al usuario
				$query_users = $this->db->query("SELECT * FROM user WHERE email COLLATE UTF8_GENERAL_CI LIKE '%".$query."%' AND status = 1 ORDER BY iduser DESC");
				
				//Verificamos si tiene usuarios
				if ($query_users->num_rows() > 0)
				{
					//Generamos el HTML
					$html = '';
					$html.= '<div class="col s12 m12 l12">';
					$html.= '	<ul class="collapsible" data-collapsible="accordion">';
					foreach ($query_users->result() as $user)
					{
						$html.= '		<li>';
						$html.= '	    	<div class="collapsible-header"><i class="material-icons">account_circle</i>'. $user->name . ' ' . $user->lastname . ' ( ' . $user->email . ' )</div>';
						//Consultamos sus puntos 
						$query_logs = $this->db->query("SELECT * FROM log WHERE log.iduser = '" . $user->iduser . "' AND log.status = 1");
						$html.= '			<div class="collapsible-body teak white-text">';
						if ($query_logs->num_rows() > 0)
						{
							$html.= '			<table class="centered responsive-table" id="table_historial">';
							$html.= '				<thead>';
							$html.= '					<tr>';
							$html.= '						<th class="gotham-bold white-text">Acción</th>';
							$html.= '						<th class="gotham-bold white-text">Valor</th>';
							$html.= '						<th class="gotham-bold white-text">Puntos</th>';
							$html.= '						<th class="gotham-bold white-text">Fecha</th>';
							$html.= '						<th class="gotham-bold white-text">IP</th>';
							$html.= '					</tr>';
							$html.= '				</thead>';
							$html.= '				<tbody>';
							foreach ($query_logs->result() as $log)
							{
								$html.= '				<tr>';
								$html.= '					<td class="gotham-book white-text">';
								switch ($log->action) {
									case 'invite': $html.= 'Share Invitación'; break;
									case 'code': $html.= 'Código Registrado'; break;
								}
								$html.= '					</td>';
								$html.= '					<td class="gotham-book white-text">'.$log->value.'</td>';
								$html.= '					<td class="gotham-book white-text">'.$log->points.' pts</td>';
								$html.= '					<td class="gotham-book white-text">'.$log->createdAt.'</td>';
								$html.= '					<td class="gotham-book white-text">'.$log->ip.'</td>';
					    		$html.= '				</tr>';
					    	}
							$html.= '				</tbody>';
							$html.= '			</table>';
						} 
						else 
						{
							$html.= '			<p class="gotham-book white-text">No tiene registrado ningún punto aún.</p>';
						}
						$html.= '			</div>';
						$html.= '	    </li>';
					}
					$html.= '	</ul>';
					$html.= '</div>';
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $html
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'no data',
						'data' => '<span class="gotham-bold white-text">No existen participantes con ese correo electrónico, intenta con otro correo electrónico.</span>'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'no data',
					'data' => '<span class="gotham-bold white-text">No existen participantes con ese correo electrónico. Recarga la página para reiniciar la búsqueda.</span>'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//resendVerify
		if ($msg == 'resendVerify')
		{
			//Leemos los datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Consultamos al Usuario
				$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Leemos el objeto
					$row = $query->row();
					
					//ENVIAMOS CORREO DE BIENVENIDA
					$name_array = explode(' ', $row->name);
					$data['name'] = $name_array[0];
					$data['code'] = $row->code;
					$email = $row->email;
					
					//Correo de Notificación
					$subject = 'Bienvenida al #RetoActivia';
					$body = $this->load->view('mail/bienvenida', $data, TRUE);
					$altbody = strip_tags($body);
			
					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
				        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
				        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email);
				        //$mail->AddBCC('milio.hernandez@gmail.com');
			
				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Verificación reenviada exitosamente.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este participante esta deshabilitado.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Ocurrió un error, intenta más tarde.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//verifyFacebook
		if ($msg == 'verifyFacebook')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$lastname = (isset($fields['lastname'])) ? (string)trim($fields['lastname']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Actualizamos los datos del usuario
				$data = array(
					'name' => $name,
					'lastname' => $lastname,
					'email' => $email,
					'verified' => 1,
					'updatedAt' => date('Y-m-d H:i:s')
				);
				$this->db->where('iduser', $iduser);
				$this->db->update('user', $data);
				
				//Consultamos los Datos del Usuario
				$query = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
				$row = $query->row();
				
				//Creamos la SESSION
				$data = array(
					'iduser' => $row->iduser,
					'email' => $row->email,
				    'name' => $row->name,
				    'lastname' => $row->lastname,
				    'code' => $row->code,
				    'verified' => $row->verified
				);

				//Save Admin in $_SESSION
				$this->session->set_userdata('user', $data);
				$this->session->set_userdata('ra_logged', true);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Ocurrió un error, intenta más tarde.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//usersDay
		if ($msg == 'usersDay')
		{
			//Leemos la Fecha
			$date = (isset($fields['date'])) ? (string)trim($fields['date']) : '';
			
			//Checamos
			if ($date)
			{
				//Generamos la Consulta
				$query = $this->db->query("SELECT * FROM user WHERE user.createdAt BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' AND user.status = 1");
				$html = '';
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Generamos el HTML
					$html.= '<table class="white">';
					$html.= '   <thead>';
					$html.= '		<tr>';
					$html.= '			<th data-field="name">Nombre</th>';
					$html.= '			<th data-field="email">Email</th>';
					$html.= '			<th data-field="facebook">Facebook</th>';
					$html.= '			<th data-field="verified">Verificado</th>';
					$html.= '		</tr>';
					$html.= '	</thead>';
					
					$html.= '	<tbody>';
					foreach ($query->result() as $row)
					{
						$html.= '	<tr>';
						$html.= '		<td>'. $row->name . ' ' . $row->lastname. '</td>';
						$html.= '		<td>'. $row->email. '</td>';
						$html.= '		<td>';
						if ($row->fbid) { $html.='<a href="https://fb.com/'.$row->fbid.'" target="_blank">Ver perfil</a>'; }
						else { $html.= 'No aplica.'; }
						$html.= '		</td>';
						$html.= '		<td>';
						if ($row->verified) { $html.= 'SI'; }
						else { $html.= 'NO'; }
						$html.= '		</td>';
						$html.= '	</tr>';
					}
					$html.= '	</tbody>';
					$html.= '</table>';
					
					//Mostrar Error
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $html
					);
					
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Ocurrió un error, intenta más tarde.',
						'data' => '<span class="gotham-book white-text">No hay participantes registrados en este día.</span>'
					);
					
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Ocurrió un error, intenta más tarde.',
					'data' => '<span class="gotham-book white-text">No existen resultados.</span>'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//codesDay
		if ($msg == 'codesDay')
		{
			//Leemos la Fecha
			$date = (isset($fields['date'])) ? (string)trim($fields['date']) : '';
			
			//Checamos
			if ($date)
			{
				//Generamos la Consulta
				$query = $this->db->query("SELECT (SELECT CONCAT(name,' ',lastname) FROM user WHERE user.iduser = log.iduser) as Nombre, (SELECT email FROM user WHERE user.iduser = log.iduser) as Email, log.value as Codigo, log.ip as IP, log.createdAt as FechaRegistro, log.product as Producto FROM log WHERE createdAt BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' AND log.action != 'invite'");
				$html = '';
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Generamos el HTML
					$html.= '<table class="white">';
					$html.= '   <thead>';
					$html.= '		<tr>';
					$html.= '			<th data-field="name">Nombre</th>';
					$html.= '			<th data-field="email">Email</th>';
					$html.= '			<th data-field="code">Código</th>';
					$html.= '			<th data-field="product">Producto</th>';
					$html.= '			<th data-field="date">FechaRegistro</th>';
					$html.= '		</tr>';
					$html.= '	</thead>';
					
					$html.= '	<tbody>';
					foreach ($query->result() as $row)
					{
						$html.= '	<tr>';
						$html.= '		<td>'. $row->Nombre. '</td>';
						$html.= '		<td>'. $row->Email. '</td>';
						$html.= '		<td>'. $row->Codigo. '</td>';
						$html.=	'		<td>'. $row->Producto. '</td>';
						$html.= '		<td>'. $row->FechaRegistro. '</td>';
						$html.= '	</tr>';
					}
					$html.= '	</tbody>';
					$html.= '</table>';
					
					//Mostrar Error
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $html
					);
					
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Ocurrió un error, intenta más tarde.',
						'data' => '<span class="gotham-book white-text">No hay códigos registrados en este día.</span>'
					);
					
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Ocurrió un error, intenta más tarde.',
					'data' => '<span class="gotham-book white-text">No existen resultados.</span>'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// assignCode
		if ($msg == 'assignCode')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$producto = (isset($fields['producto'])) ? (string)trim($fields['producto']) : '';
			$nombre_producto = (isset($fields['nombre_producto'])) ? (string)trim($fields['nombre_producto']) : '';
			$fecha = (isset($fields['fecha'])) ? (string)trim($fields['fecha']) : '';
			$fecha = strtoupper($fecha);
			$folio = (isset($fields['folio'])) ? (string)trim($fields['folio']) : '';
			$folio = strtoupper($folio);
			$count_folio = strlen($folio); $count_folio = $count_folio-3;
			$folio_final = mb_substr($folio,$count_folio,3);
			$hora = (isset($fields['hora'])) ? (string)trim($fields['hora']) : '';
			$hora = strtoupper($hora);
			$ip = '0.0.0.0';
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $ip = $_SERVER['REMOTE_ADDR'];
			}
			
			//Read Session
			$logged = (isset($_SESSION['raa_logged'])) ? $_SESSION['raa_logged'] : false;
			
			//Verificamos que sea session de admin
			if ($logged)
			{
				//Verificamos los Datos
				if ($iduser && $producto && $fecha && $folio && $hora)
				{
					//Consultamos al usuario
					$query_user = $this->db->query("SELECT * FROM user WHERE user.iduser = " . $iduser . " AND user.status = 1 LIMIT 1");
					
					//Verificamos
					if ($query_user->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row_user = $query_user->row();
						
						//Calculamos los puntos
						$puntos = 0;
						switch ($producto)
						{
							case 'bebible': $puntos = 500; break;
							case 'semillas': $puntos = 500; break;
							case 'cereales': $puntos = 400; break;
							case 'kilo': $puntos = 1000; break;
							case 'vasito': $puntos = 250; break;
							default: $puntos = 0; break;
						}
						
						//Verificamos los puntos asignados
						if ($puntos)
						{
							//Consultamos el código
							$query_code = $this->db->query("SELECT * FROM code WHERE code.date = '" . $fecha . "' AND code.folio = '". $folio_final . "' AND code.start <= '" . $hora . "' AND code.end > '" . $hora . "' AND code.status = 1 LIMIT 1");
							
							//Verificamos que exista el codigo
							if ($query_code->num_rows() > 0)
							{
								//Leemos el código
								$row_code = $query_code->row();
								
								//Verificamos puntos del producto seleccionado con los puntos en base de datos
								if ($puntos == $row_code->points)
								{
									//Consultamos si el código no ha sido utilizado más de 3 veces por el mismo usuario
									$query_log = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.value = '" . $fecha . " " . $folio . " " . $hora . "' AND log.status = 1");
									
									//Verificamos el número de códigos repetidos por usuario
									//if ($query_log->num_rows() < 3)
									if (true)
									{
										//Creamos el registro de puntos
										$data = array(
											'iduser' => $iduser,
											'points' => $row_code->points,
											'action' => 'assign',
											'value' => $fecha . ' ' . $folio . ' ' . $hora,
											'product' => $nombre_producto,
											'ip' => $ip,
											'createdAt' => date('Y-m-d H:i:s'),
											'status' => 1
										);
										$this->db->insert('log', $data);
										$idcode = $this->db->insert_id();
										
										//ENVIAMOS CORREO DE PUNTOS
										$subject = 'Se ha registrado un código a tu nombre de #RetoActivia';
										$body = $this->load->view('mail/assign', $data, TRUE);
										$altbody = strip_tags($body);
										$email = $row_user->email;
								
										//Verificamos
										if ($subject != '' && $body != '' && $altbody != '' && $email != '')
										{
											//Mandamos el Correo
											$mail = new PHPMailer(true);
											$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
											$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
											$mail->CharSet = 'UTF-8';
											$mail->Encoding = "quoted-printable";
									        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
									        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
									        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
									        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
									        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
									        $mail->Username   = "AKIAJ66WJXNG5RM4ZYWA";  // la cuenta de correo GMail
									        $mail->Password   = "ApxOe6Tk8k0u0sY1SBZNrbqSEHLyF86hW5frWukgO+b4";            // password de la cuenta GMail
									        $mail->SMTPDebug  = 0;
									        $mail->SetFrom("contacto@retoactivia.mx", utf8_encode('Reto Activia'));  //Quien envía el correo
									        $mail->AddReplyTo("contacto@retoactivia.mx", utf8_encode("Reto Activia"));  //A quien debe ir dirigida la respuesta
									        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
									        $mail->Body      = $body;
									        $mail->AltBody    = $altbody;
									        $mail->AddAddress($email);
									        //$mail->AddBCC('milio.hernandez@gmail.com');
								
									        if ($mail->Send())
											{
												$response = true;
											}
											else
											{
												$response = false;
											}
										}
										
										//Consultamos sus puntos en base de datos
										$puntos_invitaciones = 0;
										$puntos_codigos = 0;
										
										//Consultamos los Puntos por invitaciones
										$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
										
										//Calculamos los Puntos por invitaciones
										if ($query_invitaciones->num_rows() > 5)
										{
											//Asignamos el monto máximo de invitaciones
											$puntos_invitaciones = 500;
										}
										else
										{
											//Calculamos los puntos por invitación
											$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
										}
										
										//Consultamos los Puntos por Códigos
										$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
										
										//Procesamos los Puntos
										foreach ($query_puntos->result() as $row_puntos)
										{
											//Acumulamos
											$puntos_codigos = $puntos_codigos + $row_puntos->points;
										}
										
										//Asignamos los puntos
										$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
										
										//Generamos el Arreglo
										$array = array(
											'status' => (int)1,
											'msg' => 'El código ha sido registrado exitosamente.',
											'data' => $data
										);
					
										//Imprimimos el Arreglo
										$this->printJSON($array);
										$output = TRUE;
									}
									else
									{
										//Consultamos sus puntos en base de datos
										$puntos_invitaciones = 0;
										$puntos_codigos = 0;
										
										//Consultamos los Puntos por invitaciones
										$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
										
										//Calculamos los Puntos por invitaciones
										if ($query_invitaciones->num_rows() > 5)
										{
											//Asignamos el monto máximo de invitaciones
											$puntos_invitaciones = 500;
										}
										else
										{
											//Calculamos los puntos por invitación
											$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
										}
										
										//Consultamos los Puntos por Códigos
										$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
										
										//Procesamos los Puntos
										foreach ($query_puntos->result() as $row_puntos)
										{
											//Acumulamos
											$puntos_codigos = $puntos_codigos + $row_puntos->points;
										}
										
										//Asignamos los puntos
										$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
										
										//Creamos el registro de error
										$data = array(
											'iduser' => $iduser,
											'points' => $puntos,
											'action' => 'repeated',
											'value' => $fecha . ' ' . $folio . ' ' . $hora,
											'product' => $nombre_producto,
											'ip' => $ip,
											'createdAt' => date('Y-m-d H:i:s'),
											'status' => 1
										);
										$this->db->insert('fail', $data);
								
										//Mostrar Error
										$array = array(
											'status' => (int)4,
											'msg' => (string)'Código repetido.',
											'data' => $data
										);
						
										//Imprimimos el Arreglo
										$this->printJSON($array);
										$output = TRUE;
									}
								}
								else
								{
									//Consultamos sus puntos en base de datos
									$puntos_invitaciones = 0;
									$puntos_codigos = 0;
									
									//Consultamos los Puntos por invitaciones
									$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
									
									//Calculamos los Puntos por invitaciones
									if ($query_invitaciones->num_rows() > 5)
									{
										//Asignamos el monto máximo de invitaciones
										$puntos_invitaciones = 500;
									}
									else
									{
										//Calculamos los puntos por invitación
										$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
									}
									
									//Consultamos los Puntos por Códigos
									$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
									
									//Procesamos los Puntos
									foreach ($query_puntos->result() as $row_puntos)
									{
										//Acumulamos
										$puntos_codigos = $puntos_codigos + $row_puntos->points;
									}
									
									//Asignamos los puntos
									$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
									
									//Creamos el registro de error
									$data = array(
										'iduser' => $iduser,
										'points' => $puntos,
										'action' => 'assign',
										'value' => $fecha . ' ' . $folio . ' ' . $hora,
										'product' => $nombre_producto,
										'ip' => $ip,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('fail', $data);
									
									//Mostrar Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'El producto seleccionado no corresponde al folio ingresado, intenta con otro.',
										'data' => $data
									);
					
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Consultamos sus puntos en base de datos
								$puntos_invitaciones = 0;
								$puntos_codigos = 0;
								
								//Consultamos los Puntos por invitaciones
								$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
								
								//Calculamos los Puntos por invitaciones
								if ($query_invitaciones->num_rows() > 5)
								{
									//Asignamos el monto máximo de invitaciones
									$puntos_invitaciones = 500;
								}
								else
								{
									//Calculamos los puntos por invitación
									$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
								}
								
								//Consultamos los Puntos por Códigos
								$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
								
								//Procesamos los Puntos
								foreach ($query_puntos->result() as $row_puntos)
								{
									//Acumulamos
									$puntos_codigos = $puntos_codigos + $row_puntos->points;
								}
								
								//Asignamos los puntos
								$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
								
								//Creamos el registro de error
								$data = array(
									'iduser' => $iduser,
									'points' => $puntos,
									'action' => 'assign',
									'value' => $fecha . ' ' . $folio . ' ' . $hora,
									'product' => $nombre_producto,
									'ip' => $ip,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('fail', $data);
										
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Este código no es válido, intenta con otro.',
									'data' => $data
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Consultamos sus puntos en base de datos
							$puntos_invitaciones = 0;
							$puntos_codigos = 0;
							
							//Consultamos los Puntos por invitaciones
							$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
							
							//Calculamos los Puntos por invitaciones
							if ($query_invitaciones->num_rows() > 5)
							{
								//Asignamos el monto máximo de invitaciones
								$puntos_invitaciones = 500;
							}
							else
							{
								//Calculamos los puntos por invitación
								$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
							}
							
							//Consultamos los Puntos por Códigos
							$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
							
							//Procesamos los Puntos
							foreach ($query_puntos->result() as $row_puntos)
							{
								//Acumulamos
								$puntos_codigos = $puntos_codigos + $row_puntos->points;
							}
							
							//Asignamos los puntos
							$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
							
							//Creamos el registro de error
							$data = array(
								'iduser' => $iduser,
								'points' => $puntos,
								'action' => 'assign',
								'value' => $fecha . ' ' . $folio . ' ' . $hora,
								'ip' => $ip,
								'createdAt' => date('Y-m-d H:i:s'),
								'status' => 1
							);
							$this->db->insert('error', $data);
										
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Producto no válido.',
								'data' => $data
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Consultamos sus puntos en base de datos
						$puntos_invitaciones = 0;
						$puntos_codigos = 0;
						
						//Consultamos los Puntos por invitaciones
						$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
						
						//Calculamos los Puntos por invitaciones
						if ($query_invitaciones->num_rows() > 5)
						{
							//Asignamos el monto máximo de invitaciones
							$puntos_invitaciones = 500;
						}
						else
						{
							//Calculamos los puntos por invitación
							$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
						}
						
						//Consultamos los Puntos por Códigos
						$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
						
						//Procesamos los Puntos
						foreach ($query_puntos->result() as $row_puntos)
						{
							//Acumulamos
							$puntos_codigos = $puntos_codigos + $row_puntos->points;
						}
						
						//Asignamos los puntos
						$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
						
						//Creamos el registro de error
						$data = array(
							'iduser' => '0',
							'points' => '0',
							'action' => 'assign',
							'value' => $fecha . ' ' . $folio . ' ' . $hora,
							'ip' => $ip,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('error', $data);
										
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario esta deshabilitado.',
							'data' => $data
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Consultamos sus puntos en base de datos
					$puntos_invitaciones = 0;
					$puntos_codigos = 0;
					
					//Consultamos los Puntos por invitaciones
					$query_invitaciones = $this->db->query("SELECT * FROM user WHERE user.invite = '" . $row_user->code . "' AND status = 1");
					
					//Calculamos los Puntos por invitaciones
					if ($query_invitaciones->num_rows() > 5)
					{
						//Asignamos el monto máximo de invitaciones
						$puntos_invitaciones = 500;
					}
					else
					{
						//Calculamos los puntos por invitación
						$puntos_invitaciones = 100 * $query_invitaciones->num_rows();
					}
					
					//Consultamos los Puntos por Códigos
					$query_puntos = $this->db->query("SELECT * FROM log WHERE log.iduser = " . $iduser . " AND log.action = 'code' AND log.status = 1");
					
					//Procesamos los Puntos
					foreach ($query_puntos->result() as $row_puntos)
					{
						//Acumulamos
						$puntos_codigos = $puntos_codigos + $row_puntos->points;
					}
					
					//Asignamos los puntos
					$data['puntos'] = $puntos_invitaciones + $puntos_codigos;
					
					//Creamos el registro de error
					$data = array(
						'iduser' => $iduser,
						'points' => '0',
						'action' => 'assign',
						'value' => $fecha . ' ' . $folio . ' ' . $hora,
						'ip' => $ip,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('error', $data);
										
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Tienes que llenar todos los campos.',
						'data' => $data
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Llamada solo válida para administradores.',
					'data' => $data
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.'
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}

}