
	$( document ).ready(function() {

		//Read Values
		var base_url = $('#base_url').val();

		// Initialize collapse button
		$(".button-collapse").sideNav();
		// Initialize collapsible (uncomment the line below if you use the dropdown variation)
		//$('.collapsible').collapsible();
		
		$('.collapsible').collapsible();
		
		//Tooltip
		$('.tooltipped').tooltip({delay: 50});
		
		//Time Entry
		$('#inputHora').timeEntry({show24Hours:true,spinnerImage: '',defaultTime:'00:00'});
		
		//.dashboard .info hover
		$('.dashboard .info').hover(function(e) {
			//Show Info Text
			$('.dashboard .info-text').show();
		}, function(e) {
			$('.dashboard .info-text').hide();
  		});
  				
		//Modal Setup
		$('.modal').modal({
			dismissible: true, // Modal can be dismissed by clicking outside of the modal
			opacity: .5, // Opacity of modal background
			in_duration: 300, // Transition in duration
			out_duration: 200, // Transition out duration
			starting_top: '4%', // Starting top style attribute
			ending_top: '10%', // Ending top style attribute
			ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
				/*alert("Ready");*/
				console.log(modal, trigger);
      		},
	  		complete: function() { /*alert('Closed');*/ } // Callback for Modal close
    	});
		
		//formDevolucion validate
		$('#formDevolucion').validate({
			rules: {
				inputNombre: {
					required: true
				},
				inputApellidoPaterno: {
					required: true
				},
				inputApellidoMaterno: {
					required: true
				},
				inputCorreoElectronico: {
					required: true,
					email: true
				},
				inputTelefono: {
					required: true
				},
				inputGenero: {
					required: true
				},
				inputEdad: {
					required: true,
					digits: true
				},
				inputCalle: {
					required: true
				},
				inputNoExterior: {
					required: true
				},
				inputColonia: {
					required: true
				},
				inputDelegacionMunicipio: {
					required: true
				},
				inputCiudad: {
					required: true
				},
				inputEstado: {
					required: true
				},
				inputCodigoPostal: {
					required: true,
					digits: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnEnviarDevolucion Click
		$('#btnEnviarDevolucion').on('click', function(e) {
			e.preventDefault();
			
			//Regresamos al punto inicial
			$('.devoluciones .invalid').hide();
			
			//Check formDevolucion Valid
			if ($('#formDevolucion').valid())
			{
				//Read Form Data
				var inputNombre = $('#inputNombre').val();
				var inputApellidoPaterno = $('#inputApellidoPaterno').val();
				var inputApellidoMaterno = $.trim($('#inputApellidoMaterno').val());
				var inputCorreoElectronico = $.trim($('#inputCorreoElectronico').val());
				var inputTelefono = $('#inputTelefono').val();
				var inputGenero = $('#inputGenero').val();
				var inputEdad = $('#inputEdad').val();
				var inputCalle = $('#inputCalle').val();
				var inputNoExterior = $('#inputNoExterior').val();
				var inputNoInterior = $('#inputNoInterior').val();
				var inputColonia = $('#inputColonia').val();
				var inputDelegacionMunicipio = $('#inputDelegacionMunicipio').val();
				var inputCiudad = $('#inputCiudad').val();
				var inputEstado = $('#inputEstado').val();
				var inputCodigoPostal = $('#inputCodigoPostal').val();
				
				//Disable Button
				$('#btnEnviarDevolucion').prop( 'disabled', true );
				$('#btnEnviarDevolucion').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api/sendDevolucion', { inputNombre: inputNombre, inputApellidoPaterno: inputApellidoPaterno, materno: inputApellidoMaterno, email: inputCorreoElectronico, inputTelefono: inputTelefono, inputGenero: inputGenero, inputEdad: inputEdad, inputCalle: inputCalle, inputNoExterior: inputNoExterior, inputNoInterior: inputNoInterior, inputColonia: inputColonia, inputDelegacionMunicipio: inputDelegacionMunicipio, inputCiudad: inputCiudad, inputEstado: inputEstado, inputCodigoPostal: inputCodigoPostal }).done(function( data ) {
					//Check Status Call
					if (data == 'success')
					{
						//Show Message
						$('#formDevolucion').html('<div class="space100"></div><span class="gotham-book block font20 sherwood-text">Hemos recibido tus datos correctamente. Te contactaremos a la brevedad.</span>');
					}
					else
					{
						//Show Error
						Materialize.toast("Ocurrió un error, intenta más tarde.", 4000);

						//Enable Button
						$('#btnEnviarDevolucion').prop( 'disabled', false );
		                $('#btnEnviarDevolucion').html('Enviar');
					}
				});
			}
			else
			{
				//Mostramos el Error
				$('.devoluciones .invalid').fadeIn('slow');
			}
			
			return false;
		});
		
		//formLogin validate
		$('#formLogin').validate({
			rules: {
				inputLoginEmail: {
					required: true,
					email: true
				},
				inputLoginPassword: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnLogin Click
		$('#btnLogin').on('click', function(e) {
			e.preventDefault();
			
			//Check Form Validate
	  		if ($('#formLogin').valid())
	  		{		  		
		  		//Read Data
		  		var email = $.trim($('#inputLoginEmail').val());
		  		var password = $('#inputLoginPassword').val();
		  		var param = '{"msg": "loginUser","fields": {"email": "' + email + '", "password": "' + password + '"}}';
		  		
		  		//Disable Button
				$('#btnLogin').prop( 'disabled', true );
				$('#btnLogin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Gracias
						window.location.href = base_url + 'dashboard/';
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('#btnLogin').prop( 'disabled', false );
		                $('#btnLogin').html('Ingresar');
					}
				});
		  	}
			
			return false;
		});
		
		//formRegister validate
		$('#formRegister').validate({
			rules: {
				inputName: {
					required: true
				},
				inputLastName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnRegister Click
		$('#btnRegister').on('click', function(e) {
			e.preventDefault();
			
			//Check Form Validate
	  		if ($('#formRegister').valid())
	  		{
		  		//Read Data 
		  		var terms = $('#inputTerms').prop( "checked" );
		  		var privacy = $('#inputPrivacy').prop( "checked" );	
		  		
		  		//Check Terms & Privacy
		  		if (terms && privacy)
		  		{
			  		//Read Data
			  		var name = $.trim($('#inputName').val());
			  		var lastname = $.trim($('#inputLastName').val());
			  		var email = $.trim($('#inputEmail').val());
			  		var password = $('#inputPassword').val();
			  		var invite = $('#inputInvite').val();
			  		var param = '{"msg": "createUser","fields": {"name": "' + name + '", "lastname": "' + lastname + '", "email": "' + email + '", "password": "' + password + '", "invite": "' + invite + '"}}';
			  		
			  		//Disable Button
					$('#btnRegister').prop( 'disabled', true );
					$('#btnRegister').html('PROCESANDO...');
	
					//API Call
					$.post(base_url + 'api', { param: param }).done(function( data ) {
						//Check Status Call
						if (data.status == 1)
						{
							//Redirect to Gracias
							window.location.href = base_url + 'gracias/';
						}
						else
						{
							//Show Error
							Materialize.toast(data.msg, 4000);
	
							//Enable Button
							$('#btnRegister').prop( 'disabled', false );
			                $('#btnRegister').html('Enviar');
						}
					});
		  		}
		  		else
		  		{
			  		//Check Terms
			  		if (!terms) { Materialize.toast("Debes de aceptar los términos y condiciones.", 4000); }
			  		
			  		//Check Privacy
			  		if (!privacy) { Materialize.toast("Debes de aceptar las políticas de privacidad.", 4000); }
		  		}
		  	}
			
			return false;
		});
		
		//btnShareTwitter Click
		$('.btnShareTwitter').on('click', function(e) {
			e.preventDefault();
			
			var code = $(this).attr('rel');
	        var width  = 575,
	            height = 400,
	            left   = ($(window).width()  - width)  / 2,
	            top    = ($(window).height() - height) / 2,
	            url    = 'http://twitter.com/share?url=' + base_url + 'invite/' + code + '&text=¡Ya me registré en el RetoActivia! ¿y tú ya estás lista para cuidarte y ganar?',
	            opts   = 'status=1' +
	                     ',width='  + width  +
	                     ',height=' + height +
	                     ',top='    + top    +
	                     ',left='   + left;

	        window.open(url, 'twitter', opts);

	        return false;
	    });

		//btnShareFacebook Click
	    $('.btnShareFacebook').on('click', function(e) {
		    e.preventDefault();
			
			var code = $(this).attr('rel');
	        var width  = 575,
	            height = 400,
	            left   = ($(window).width()  - width)  / 2,
	            top    = ($(window).height() - height) / 2,
	            url    = 'http://www.facebook.com/sharer.php?s=100&p[url]=' + base_url + '/invite/' + code,
	            opts   = 'status=1' +
	                     ',width='  + width  +
	                     ',height=' + height +
	                     ',top='    + top    +
	                     ',left='   + left;

	        window.open(url, 'facebook', opts);

	        return false;
	    });
	    
	    //formReset validate
		$('#formReset').validate({
			rules: {
				inputResetEmail: {
					required: true,
					email: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnReset Click
		$('#btnReset').on('click', function(e) {
			e.preventDefault();
			
			//Check Form Validate
	  		if ($('#formReset').valid())
	  		{		  		
		  		//Read Data
		  		var email = $.trim($('#inputResetEmail').val());
		  		var param = '{"msg": "resetPassword","fields": {"email": "' + email + '"}}';
		  		
		  		//Disable Button
				$('#btnReset').prop( 'disabled', true );
				$('#btnReset').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Show Error
					Materialize.toast(data.msg, 4000);

					//Enable Button
					$('#btnReset').prop( 'disabled', false );
	                $('#btnReset').html('Enviar');
				});
		  	}
			
			return false;
		});
		
		//formNewPassword Validate
		$('#formNewPassword').validate({
			rules: {
				inputNewPassword: {
					required: true
				},
				inputConfirmNewPassword: {
					equalTo: '#inputNewPassword'
				}
			}
		});
		
		//btnNewPassword Click
		$('#btnNewPassword').on('click', function(e) {
			e.preventDefault();
			
			//Check Form Validate
	  		if ($('#formNewPassword').valid())
	  		{		  		
		  		//Read Data
		  		var token = $.trim($('#token').val());
		  		var password = $.trim($('#inputNewPassword').val());
		  		var param = '{"msg": "newPassword","fields": {"token": "' + token + '", "password": "' + password + '"}}';
		  		
		  		//Disable Button
				$('#btnNewPassword').prop( 'disabled', true );
				$('#btnNewPassword').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Show Error
					Materialize.toast(data.msg, 4000);

					//Enable Button
					$('#btnNewPassword').prop( 'disabled', false );
	                $('#btnNewPassword').html('Actualizar Contraseña');
				});
		  	}
			
			return false;
		});
		
		//invite_code Click
		$('#invite_code').on('click', function(e) {
			e.preventDefault();
			
			$("#code").select();
		    document.execCommand('copy');
		    
		    Materialize.toast("Enlace único copiado.", 4000);
			
			return false;
		});
		
		//formCode validate
		$('#formCode').validate({
			rules: {
				inputProducto: {
					required: true
				},
				inputDate: {
					required: true,
					minlength: 7,
					maxlength: 7
				},
				inputFolio: {
					required: true,
					minlength: 3,
					maxlength: 4
				},
				inputHora: {
					required: true,
					minlength: 5,
					maxlength: 5
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnCode Click
		$('#btnCode').on('click', function(e) {
			e.preventDefault();
			
			//Verificamos
			if ($('#formCode').valid())
			{
				//Leemos los Datos
				var iduser = $.trim($('#iduser').val());
				var producto = $.trim($('#inputProducto').val());
				var producto_string = $('#inputProducto option:selected').text();
				var fecha = $.trim($('#inputDate').val());
				var folio = $.trim($('#inputFolio').val());
				var hora = $.trim($('#inputHora').val());
				var param = '{"msg": "addCode","fields": {"iduser": "' + iduser + '", "producto": "' + producto + '", "nombre_producto": "' + producto_string + '", "fecha": "' + fecha + '", "folio": "' + folio + '", "hora": "' + hora + '"}}';
		  		$('#codigo_error').hide();
		  		
		  		//Disable Button
				$('#btnCode').prop( 'disabled', true );
				$('#btnCode').html('PROCESANDO...');

				//Revisamos si son bebibles
				if (producto == 'bebible')
				{ 
					if (folio.match(/^(A|B|L|a|b|l)/)) {
						//API Call
						$.post(base_url + 'api', { param: param }).done(function( data ) {
							//Show Error
							Materialize.toast(data.msg, 4000);
							
							//Verificamos error
							if (data.status == 4) { $('#codigo_error').fadeIn('slow'); }
							else 
							{ 
								//Asignamos los puntos
								$('#dashboard_points').text(data.data.puntos); 
								
								//Limpiamos el Formulario
								$('#inputProducto').prop('selectedIndex',0);
								$('#inputDate').val("");
								$('#inputFolio').val("");
								$('#inputHora').val("");
							}
		
							//Enable Button
							$('#btnCode').prop( 'disabled', false );
			                $('#btnCode').html('Añadir');
						});
					}
					else
					{
						//Mostramos el Error
						Materialize.toast('Este código no es válido, intenta con otro.', 4000);
						
						//Enable Button
						$('#btnCode').prop( 'disabled', false );
		                $('#btnCode').html('Añadir');
					}
				}
				else
				{
					//API Call
					$.post(base_url + 'api', { param: param }).done(function( data ) {
						//Show Error
						Materialize.toast(data.msg, 4000);
						
						//Verificamos error
						if (data.status == 4) { $('#codigo_error').fadeIn('slow'); }
						else 
						{ 
							//Asignamos los puntos
							$('#dashboard_points').text(data.data.puntos); 
							
							//Limpiamos el Formulario
							$('#inputProducto').prop('selectedIndex',0);
							$('#inputDate').val("");
							$('#inputFolio').val("");
							$('#inputHora').val("");
						}
	
						//Enable Button
						$('#btnCode').prop( 'disabled', false );
		                $('#btnCode').html('Añadir');
					});
				}
			}
			
			return false;
		});
		
		//formAdmin validate
		$('#formAdmin').validate({
			rules: {
				inputAdminUsername: {
					required: true,
				},
				inputAdminPassword: {
					required: true,
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnAdmin Click
		$('#btnAdmin').on('click', function(e) {
			e.preventDefault();
			
			//Verificamos
			if ($('#formAdmin').valid())
			{
				//Read Data
		  		var username = $.trim($('#inputAdminUsername').val());
		  		var password = $('#inputAdminPassword').val();
		  		var param = '{"msg": "loginAdmin","fields": {"username": "' + username + '", "password": "' + password + '"}}';
		  		
		  		//Disable Button
				$('#btnAdmin').prop( 'disabled', true );
				$('#btnAdmin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Gracias
						window.location.href = base_url + 'users/dashboard/';
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('#btnAdmin').prop( 'disabled', false );
		                $('#btnAdmin').html('Ingresar');
					}
				});
			}
			
			return false;
		});
		
		//formSearch input 
		$('#formSearch input').keyup(function() 
		{
			//Leemos el Valor
			var query = $(this).val();
			var param = '{"msg": "queryUser","fields": {"query": "' + query + '"}}';
		  		
	  		//API Call
			$.post(base_url + 'api', { param: param }).done(function( data ) {
				//Redirect to Gracias
				$('#results').html(data.data);
				$('.collapsible').collapsible();
				$('#pagination').hide();
			});
			
		});
		
		//btnResendVerify Click
		$('#btnResendVerify').on('click', function(e) {
			e.preventDefault();
			
			//Leemos los Valores
			var iduser = $(this).attr('rel');
			var param = '{"msg": "resendVerify","fields": {"iduser": "' + iduser + '"}}';
		  		
	  		//Disable Button
			$('#btnResendVerify').prop( 'disabled', true );
			$('#btnResendVerify').html('PROCESANDO...');

			//API Call
			$.post(base_url + 'api', { param: param }).done(function( data ) {
				//Show Error
				Materialize.toast(data.msg, 4000);

				//Enable Button
				$('#btnResendVerify').prop( 'disabled', false );
                $('#btnResendVerify').html('Reenviar correo de verificación');
			});
			
			return false;
		});
		
		//formConfirmFacebook validate
		$('#formConfirmFacebook').validate({
			rules: {
				inputName: {
					required: true
				},
				inputLastName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnConfirmFacebook Click
		$('#btnConfirmFacebook').on('click', function(e) {
			e.preventDefault();
			
			//Check Form Validate
	  		if ($('#formConfirmFacebook').valid())
	  		{
		  		//Read Data 
		  		var terms = $('#inputTerms').prop( "checked" );
		  		var privacy = $('#inputPrivacy').prop( "checked" );	
		  		
		  		//Check Terms & Privacy
		  		if (terms && privacy)
		  		{
			  		//Read Data
			  		var iduser = $('#iduser').val();
			  		var name = $.trim($('#inputName').val());
			  		var lastname = $.trim($('#inputLastName').val());
			  		var email = $.trim($('#inputEmail').val());
			  		var param = '{"msg": "verifyFacebook","fields": {"iduser": "' + iduser + '", "name": "' + name + '", "lastname": "' + lastname + '", "email": "' + email + '"}}';
			  		
			  		//Disable Button
					$('#btnConfirmFacebook').prop( 'disabled', true );
					$('#btnConfirmFacebook').html('PROCESANDO...');
	
					//API Call
					$.post(base_url + 'api', { param: param }).done(function( data ) {
						//Check Status Call
						if (data.status == 1)
						{
							//Redirect to Gracias
							window.location.href = base_url + 'dashboard/';
						}
						else
						{
							//Show Error
							Materialize.toast(data.msg, 4000);
	
							//Enable Button
							$('#btnConfirmFacebook').prop( 'disabled', false );
			                $('#btnConfirmFacebook').html('Enviar');
						}
					});
		  		}
		  		else
		  		{
			  		//Check Terms
			  		if (!terms) { Materialize.toast("Debes de aceptar los términos y condiciones.", 4000); }
			  		
			  		//Check Privacy
			  		if (!privacy) { Materialize.toast("Debes de aceptar las políticas de privacidad.", 4000); }
		  		}
		  	}
			
			return false;
		});
		
		//formQuery select change
		$('#formQuery #date').on('change', function(e) {
			e.preventDefault();
			
			var date = $(this).val();
			
			if (date)
			{
				var param = '{"msg": "usersDay","fields": {"date": "' + date + '"}}';
				$('#results').html('<div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-yellow"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-green"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
			  		
		  		//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					
					if (data.status == 1)
					{
						$('#btnXLS').removeClass("disabled");	
					}
					else
					{
						$('#btnXLS').addClass("disabled");
					}
					
					$('#btnXLS').attr('href', base_url + 'users/download/' + date);
					$('#results').html(data.data);
				});
			}
			else
			{
				//Show Error
				Materialize.toast('Tienes que seleccionar una fecha válida.', 4000);
			}
			
			return false;
		});
		
		//formCodes select change
		$('#formCodes #date').on('change', function(e) {
			e.preventDefault();
			
			var date = $(this).val();
			
			if (date)
			{
				var param = '{"msg": "codesDay","fields": {"date": "' + date + '"}}';
				$('#results').html('<div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-yellow"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-green"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
			  		
		  		//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					
					if (data.status == 1)
					{
						$('#btnXLS').removeClass("disabled");	
					}
					else
					{
						$('#btnXLS').addClass("disabled");
					}
					
					$('#btnXLS').attr('href', base_url + 'users/download_codes/' + date);
					$('#results').html(data.data);
				});
			}
			else
			{
				//Show Error
				Materialize.toast('Tienes que seleccionar una fecha válida.', 4000);
			}
			
			return false;
		});
		
		//formAssign validate
		$('#formAssign').validate({
			rules: {
				inputUser: {
					required: true
				},
				inputProducto: {
					required: true
				},
				inputDate: {
					required: true,
					minlength: 7,
					maxlength: 7
				},
				inputFolio: {
					required: true,
					minlength: 3,
					maxlength: 4
				},
				inputHora: {
					required: true,
					minlength: 5,
					maxlength: 5
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnAssign Click
		$('#btnAssign').on('click', function(e) {
			e.preventDefault();
			
			if ($('#formAssign').valid())
			{
				//Leemos los Datos
				var iduser = $.trim($('#inputUser').val());
				var producto = $.trim($('#inputProducto').val());
				var producto_string = $('#inputProducto option:selected').text();
				var fecha = $.trim($('#inputDate').val());
				var folio = $.trim($('#inputFolio').val());
				var hora = $.trim($('#inputHora').val());
				var param = '{"msg": "assignCode","fields": {"iduser": "' + iduser + '", "producto": "' + producto + '", "nombre_producto": "' + producto_string + '", "fecha": "' + fecha + '", "folio": "' + folio + '", "hora": "' + hora + '"}}';
		  		
		  		//Disable Button
				$('#btnAssign').prop( 'disabled', true );
				$('#btnAssign').html('PROCESANDO...');

				//Revisamos si son bebibles
				if (producto == 'bebible')
				{ 
					if (folio.match(/^(A|B|L|a|b|l)/)) {
						//API Call
						$.post(base_url + 'api', { param: param }).done(function( data ) {
							//Show Error
							Materialize.toast(data.msg, 4000);
							
							//Limpiamos el Formulario
							$('#inputUser').prop('selectedIndex',0);
							$('#inputProducto').prop('selectedIndex',0);
							$('#inputDate').val("");
							$('#inputFolio').val("");
							$('#inputHora').val("");
		
							//Enable Button
							$('#btnAssign').prop( 'disabled', false );
			                $('#btnAssign').html('Asignar Código');
						});
					}
					else
					{
						//Mostramos el Error
						Materialize.toast('Este código no es válido, intenta con otro.', 4000);
						
						//Enable Button
						$('#btnAssign').prop( 'disabled', false );
		                $('#btnAssign').html('Asignar Código');
					}
				}
				else
				{
					//API Call
					$.post(base_url + 'api', { param: param }).done(function( data ) {
						//Show Error
						Materialize.toast(data.msg, 4000);
						
						//Limpiamos el Formulario
						$('#inputUser').prop('selectedIndex',0);
						$('#inputProducto').prop('selectedIndex',0);
						$('#inputDate').val("");
						$('#inputFolio').val("");
						$('#inputHora').val("");
	
						//Enable Button
						$('#btnAssign').prop( 'disabled', false );
		                $('#btnAssign').html('Asignar Código');
					});
				}
			}
			
			return false;
		});
		
		//formNewCodeL10 validate
		$('#formNewCodeL10').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL10 Click
		$('#btnNewCodeL10').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL10').valid())
			{
				//Submit Form
				$('#formNewCodeL10').submit();
			}
			
			return false;
		});
		
		//formNewCodeL14 validate
		$('#formNewCodeL14').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL14 Click
		$('#btnNewCodeL14').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL14').valid())
			{
				//Submit Form
				$('#formNewCodeL14').submit();
			}
			
			return false;
		});
		
		//formNewCodeL15 validate
		$('#formNewCodeL15').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL15 Click
		$('#btnNewCodeL15').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL15').valid())
			{
				//Submit Form
				$('#formNewCodeL15').submit();
			}
			
			return false;
		});
		
		//formNewCodeL17 validate
		$('#formNewCodeL17').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL17 Click
		$('#btnNewCodeL17').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL17').valid())
			{
				//Submit Form
				$('#formNewCodeL17').submit();
			}
			
			return false;
		});
		
		//formNewCodeL21 validate
		$('#formNewCodeL21').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL21 Click
		$('#btnNewCodeL21').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL21').valid())
			{
				//Submit Form
				$('#formNewCodeL21').submit();
			}
			
			return false;
		});
		
		//formNewCodeL22 validate
		$('#formNewCodeL22').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL22 Click
		$('#btnNewCodeL22').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL22').valid())
			{
				//Submit Form
				$('#formNewCodeL22').submit();
			}
			
			return false;
		});
		
		//formNewCodeL23 validate
		$('#formNewCodeL23').validate({
			rules: {
				inputDate: {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			}
		});
		
		//btnNewCodeL23 Click
		$('#btnNewCodeL23').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewCodeL23').valid())
			{
				//Submit Form
				$('#formNewCodeL23').submit();
			}
			
			return false;
		});
		
		//Resize Window
		window.onresize = function() {
			do_resize();
		}
		
		do_resize();
		
		function do_resize()
		{
			var height = $( document ).height();
	  		var width = $( document ).width();
	  		
	  		if (width > 992)
	  		{
	  			$('.home #green-side').css('height', height-95);
	  			$('.invite #green-side').css('height', height-95);
	  			$('.gracias #green-side').css('height', height-95);
	  			$('.reset #green-side').css('height', height-95);
	  			$('.historial #green-side').css('height', height-95);
	  			$('.verify #green-side').css('height', height-95);
	  			$('.request #green-side').css('height', height-95);
	  		}
	  		else
	  		{
		  		$('.home #green-side').css('height', 'auto');
		  		$('.invite #green-side').css('height', 'auto');
		  		$('.gracias #green-side').css('height', 'auto');
		  		$('.reset #green-side').css('height', 'auto');
		  		$('.historial #green-side').css('height', 'auto');
		  		$('.verify #green-side').css('height', 'auto');
		  		$('.request #green-side').css('height', 'auto');
	  		}
		}
		
	});